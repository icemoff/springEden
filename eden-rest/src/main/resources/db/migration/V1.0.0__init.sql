DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`
(
    `user_id`   bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `username`  varchar(50)  NOT NULL DEFAULT '' COMMENT '登录用户名',
    `password`  varchar(255) NOT NULL DEFAULT '' COMMENT '用户密码',
    `nick_name` varchar(50)  NOT NULL DEFAULT '' COMMENT '昵称',
    `phone`     varchar(30)  NOT NULL DEFAULT '' COMMENT '手机号码',
    `ctime`     timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `utime`     timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`user_id`)
)engine=InnoDB,charset=utf8mb4;


-- sys后台系统管理
-- 系统用户
DROP TABLE IF EXISTS `t_sys_user`;
CREATE TABLE `t_sys_user`
(
    `id`        bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `username`  varchar(50)  NOT NULL DEFAULT '' COMMENT '登录用户名',
    `password`  varchar(255) NOT NULL DEFAULT '' COMMENT '用户密码',
    `nick_name` varchar(50)  NOT NULL DEFAULT '' COMMENT '昵称',
    `phone`     varchar(30)  NOT NULL DEFAULT '' COMMENT '手机号码',
    `ctime`     timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `utime`     timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`),
    unique (username)
)engine=InnoDB,charset=utf8mb4;
INSERT INTO t_sys_user(id, username, password, nick_name, phone)
VALUES (1, 'admin', '$2a$10$ayCYPhGlFFYvy1G1raVb5eMVqEhLaIjji7bkkcuM8reWljK0yrkQC', '超级管理员', '17288888888');

-- 菜单
DROP TABLE IF EXISTS `t_sys_role`;
CREATE TABLE `t_sys_role`
(
    `id`        bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `code`      varchar(50) NOT NULL DEFAULT '' COMMENT '角色编码，不能重复',
    `role_name` varchar(50) NOT NULL DEFAULT '' COMMENT '角色名称',
    `ctime`     timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `utime`     timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`),
    unique (code)
)engine=InnoDB,charset=utf8mb4;
INSERT INTO t_sys_role(id, code, role_name)
VALUES (1, 'super-admin', '超级管理员');

-- 用户角色
DROP TABLE IF EXISTS `t_sys_userrole`;
CREATE TABLE `t_sys_userrole`
(
    `id`          bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `sys_user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户id',
    `sys_role_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '角色id',
    `ctime`       timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `utime`       timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`)
)engine=InnoDB,charset=utf8mb4;
INSERT INTO t_sys_userrole(id, sys_user_id, sys_role_id)
VALUES (1, 1, 1);

-- 菜单
DROP TABLE IF EXISTS `t_sys_menu`;
CREATE TABLE `t_sys_menu`
(
    `id`         bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `pid`        bigint(20) NOT NULL DEFAULT '0' COMMENT '父菜单ID',
    `sys_app_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '所属应用',
    `menu_name`  varchar(200) NOT NULL DEFAULT '' COMMENT '菜单名称',
    `menu_url`   varchar(300) NOT NULL DEFAULT '' COMMENT '菜单URL',
    `icon`       varchar(300) NOT NULL DEFAULT '' COMMENT '菜单图标',
    `display`    tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否显示 1-显示 0-不显示',
    `seq`  int(11) NOT NULL DEFAULT '99' COMMENT '排序,越小越前',
    `ctime`      timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `utime`      timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`)
)engine=InnoDB,charset=utf8mb4;

-- 系统应用
DROP TABLE IF EXISTS `t_sys_app`;
CREATE TABLE `t_sys_app`
(
    `id`        bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `name`      varchar(200) NOT NULL DEFAULT '' COMMENT '应用名称',
    `seq` int(11) NOT NULL DEFAULT '99' COMMENT '排序,越小越前',
    `ctime`     timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `utime`     timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`)
)engine=InnoDB,charset=utf8mb4;
INSERT INTO t_sys_app(id, name)
VALUES (0, '默认应用');

-- 角色菜单
DROP TABLE IF EXISTS `t_sys_rolemenu`;
CREATE TABLE `t_sys_rolemenu`
(
    `id`          bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `sys_role_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '角色ID',
    `sys_menu_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '菜单ID',
    `sys_app_id`  bigint(20) NOT NULL DEFAULT '0' COMMENT '菜单应用ID，冗余过来的',
    `ctime`       timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `utime`       timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`),
    INDEX         `idx_rolemenu_role` (`sys_role_id`)
)engine=InnoDB,charset=utf8mb4;

-- 角色权限
DROP TABLE IF EXISTS `t_sys_roleperm`;
CREATE TABLE `t_sys_roleperm`
(
    `id`               bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `sys_role_id`      bigint(20) NOT NULL DEFAULT '0' COMMENT '角色ID',
    `auth_module_code` varchar(50) NOT NULL DEFAULT '' COMMENT '权限模块编码',
    `perm_method_code` varchar(50) NOT NULL DEFAULT '' COMMENT '权限方法编码',
    `ctime`            timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `utime`            timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`),
    INDEX              `idx_roleperm_role` (`sys_role_id`)
)engine=InnoDB,charset=utf8mb4;

-- 字典
DROP TABLE IF EXISTS `t_sys_dict`;
CREATE TABLE `t_sys_dict`
(
    `id`        bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `dict_name` varchar(50)  NOT NULL DEFAULT '' COMMENT '字典名称',
    `dict_key`  varchar(50)  NOT NULL DEFAULT '' COMMENT '字典key',
    `status`    char(1)      NOT NULL default '0' comment '状态（0正常 1停用）',
    `remark`    varchar(500) NOT NULL DEFAULT '' comment '备注',
    `ctime`     timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `utime`     timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`),
    unique (dict_key)
)engine=InnoDB,charset=utf8mb4;

-- 字典值
DROP TABLE IF EXISTS `t_sys_dict_data`;
CREATE TABLE `t_sys_dict_data`
(
    `id`         bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `seq`        int(4) NOT NULL default 99 comment '排序',
    `dict_label` varchar(50) NOT NULL DEFAULT '' COMMENT '显示标签',
    `dict_value` varchar(50) NOT NULL DEFAULT '' COMMENT '字典值',
    `dict_key`   varchar(50) NOT NULL DEFAULT '' COMMENT '字典key',
    `ctime`      timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `utime`      timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`),
    INDEX        `idx_dictdata_key` (`dict_key`)
)engine=InnoDB,charset=utf8mb4;













