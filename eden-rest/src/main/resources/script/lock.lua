-- 获取KEY
local key = KEYS[1]

-- 获取ARGV
local timeout = ARGV[1]

local nx = redis.call("setnx",key,"lock")
if nx == 1 then
  return redis.call("expire",key,timeout)
else
  return 0
end

