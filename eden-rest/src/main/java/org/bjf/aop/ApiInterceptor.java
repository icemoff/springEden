package org.bjf.aop;

import com.alibaba.fastjson2.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.bjf.exception.CommMsgCode;
import org.bjf.exception.ServiceException;
import org.bjf.modules.core.web.core.LoginInfo;
import org.bjf.modules.core.web.core.LoginRequired;
import org.bjf.modules.core.web.core.ThreadContext;
import org.bjf.modules.user.enums.UserRedisKey;
import org.bjf.utils.ExceptionAssert;
import org.bjf.utils.RedisUtil;
import org.bjf.utils.TokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * 权限校验拦截器
 *
 * @author bjf
 */
@Component
@Slf4j
public class ApiInterceptor implements HandlerInterceptor {

    @Autowired
    private RedisUtil redis;

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) {
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            LoginRequired loginRequired = handlerMethod.getMethodAnnotation(LoginRequired.class);
            if (loginRequired == null) {
                // 没有声明权限，直接放行
                return Boolean.TRUE;
            }

            // token校验
            String accessToken = getAccessToken(request);
            if (StringUtils.isBlank(accessToken) || !TokenUtil.verifyToken(accessToken)) {
                log.error("invalid token:{}", accessToken);
                throw new ServiceException(CommMsgCode.UNAUTHORIZED);
            }
            LoginInfo loginInfo = getLoginInfo(accessToken);
            // 登录用户信息放到ThreadLocal
            loginInfo.setLastTime(new Date());
            ThreadContext.setLoginInfo(loginInfo);

            log.info("api loginInfo：" + JSON.toJSONString(loginInfo));
        }

        return Boolean.TRUE;
    }

    private LoginInfo getLoginInfo(String accessToken) {
        // redis取登录用户信息
        String userKey = UserRedisKey.TOKEN_API.as(accessToken);
        LoginInfo loginInfo = redis.getObj(userKey);
        ExceptionAssert.notNull(loginInfo, CommMsgCode.UNAUTHORIZED);

        // 更新reids过期时间
        redis.setObj(userKey, loginInfo, 86400 * 15);

        return loginInfo;
    }

    private String getAccessToken(HttpServletRequest request) {
        String accessToken = request.getHeader("x-api-token");
        if (StringUtils.isBlank(accessToken)) {
            accessToken = request.getParameter("x-api-token");
        }
        return accessToken;
    }
}
