package org.bjf.aop;

import java.lang.annotation.*;

/**
 * 多类型content-type解析注解
 *
 * @author kingdee
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MultiPostResolver {
}
