package org.bjf.modules.sys.query;

import lombok.Data;
import lombok.experimental.Accessors;
import org.bjf.modules.core.bean.Query;

/**
 * @author bjf
 */
@Data
@Accessors(chain = true)
public class SysUserQuery extends Query {

    private String username;
    private String phone;
}
