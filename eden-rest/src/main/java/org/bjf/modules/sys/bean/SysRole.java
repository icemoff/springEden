package org.bjf.modules.sys.bean;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;
import org.bjf.modules.core.bean.BaseEntity;

/**
 * 系统角色
 *
 * @author bjf
 */
@Data
@Accessors(chain = true)
@TableName("t_sys_role")
public class SysRole extends BaseEntity {

    @TableId
    private Long id;

    /**
     * 角色编码，不能重复
     */
    private String code;
    private String roleName;
}
