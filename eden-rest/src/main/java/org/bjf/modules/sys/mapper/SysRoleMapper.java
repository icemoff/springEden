package org.bjf.modules.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.bjf.modules.sys.bean.SysRole;

public interface SysRoleMapper extends BaseMapper<SysRole> {

}
