package org.bjf.modules.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.bjf.modules.sys.bean.SysRoleMenu;
import org.springframework.stereotype.Repository;

@Repository
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}
