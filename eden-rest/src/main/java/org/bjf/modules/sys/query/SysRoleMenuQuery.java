package org.bjf.modules.sys.query;

import lombok.Data;
import lombok.experimental.Accessors;
import org.bjf.modules.core.bean.Query;

import java.util.List;

/**
 * @author bjf
 */
@Data
@Accessors(chain = true)
public class SysRoleMenuQuery extends Query {

    private Long sysRoleId;
    private Long sysMenuId;
    /**
     * 角色ID，IN查询
     */
    private List<Long> roleIdList;
}
