package org.bjf.modules.sys.web.vo;

import lombok.Data;
import lombok.experimental.Accessors;
import org.bjf.modules.sys.bean.SysMenu;

/**
 * 菜单vo
 *
 * @author icemo
 */
@Data
@Accessors(chain = true)
public class MenuVO extends SysMenu {

    /**
     * 是否已分配 1-是 0-否
     */
    private int assigned;
}
