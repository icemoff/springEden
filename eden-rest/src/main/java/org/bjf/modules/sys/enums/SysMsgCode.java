package org.bjf.modules.sys.enums;

import org.bjf.exception.MsgCode;

/**
 * Created by admin on 2017/9/1.
 */
public enum SysMsgCode implements MsgCode {

    USER_NOT_EXIST(30001, "用户不存在"),
    USER_HAS_EXIST(30002, "用户已存在"),
    USER_WRONG(30003, "用户名或者密码不正确");

    private int code;
    private String message;

    SysMsgCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
