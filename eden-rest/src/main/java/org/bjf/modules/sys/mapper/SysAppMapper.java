package org.bjf.modules.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.bjf.modules.sys.bean.SysApp;

public interface SysAppMapper extends BaseMapper<SysApp> {

}
