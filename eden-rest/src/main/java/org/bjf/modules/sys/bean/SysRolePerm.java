package org.bjf.modules.sys.bean;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;
import org.bjf.modules.core.bean.BaseEntity;

/**
 * @author bjf
 */
@Data
@Accessors(chain = true)
@TableName("t_sys_roleperm")
public class SysRolePerm extends BaseEntity {

    @TableId
    private Long id;

    /**
     * 角色ID
     */
    private Long sysRoleId;
    /**
     * 权限模块编码
     */
    private String authModuleCode;
    /**
     * 权限方法编码
     */
    private String permMethodCode;
}
