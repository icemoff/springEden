package org.bjf.modules.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.bjf.modules.sys.bean.SysDict;

/**
 * @author bjf
 */
public interface SysDictMapper extends BaseMapper<SysDict> {

}
