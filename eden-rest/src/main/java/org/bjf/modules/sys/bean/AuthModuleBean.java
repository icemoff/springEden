package org.bjf.modules.sys.bean;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class AuthModuleBean {
    private String code;
    private String desc;
}
