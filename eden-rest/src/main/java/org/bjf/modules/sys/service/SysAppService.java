package org.bjf.modules.sys.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.bjf.modules.core.service.BaseService;
import org.bjf.modules.sys.bean.SysApp;
import org.bjf.modules.sys.mapper.SysAppMapper;
import org.bjf.modules.sys.query.SysAppQuery;
import org.springframework.stereotype.Service;

/**
 * @author bjf
 * @date 2017/11/27
 */
@Service
@Slf4j
public class SysAppService extends BaseService<SysAppMapper, SysApp, SysAppQuery> {


    @Override
    protected LambdaQueryWrapper<SysApp> buildQuery(SysAppQuery q) {
        if (q == null) {
            return null;
        }
        LambdaQueryWrapper<SysApp> qw = new LambdaQueryWrapper<>();
        qw.eq(q.getId() != null, SysApp::getId, q.getId());
        return qw;
    }

}
