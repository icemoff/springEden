package org.bjf.modules.sys.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.bjf.modules.core.service.BaseService;
import org.bjf.modules.sys.bean.SysMenu;
import org.bjf.modules.sys.mapper.SysMenuMapper;
import org.bjf.modules.sys.query.SysMenuQuery;
import org.springframework.stereotype.Service;

/**
 * @author bjf
 * @date 2017/11/27
 */
@Service
@Slf4j
public class SysMenuService extends BaseService<SysMenuMapper, SysMenu, SysMenuQuery> {


    @Override
    protected LambdaQueryWrapper<SysMenu> buildQuery(SysMenuQuery q) {
        if (q == null) {
            return null;
        }
        LambdaQueryWrapper<SysMenu> qw = new LambdaQueryWrapper<>();
        qw.eq(q.getPid() != null, SysMenu::getPid, q.getPid());
        qw.eq(q.getSysAppId() != null, SysMenu::getSysAppId, q.getSysAppId());
        qw.likeRight(StringUtils.isNotBlank(q.getMenuName()), SysMenu::getMenuName, q.getMenuName());
        return qw;
    }
}
