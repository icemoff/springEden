package org.bjf.modules.sys.bean;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;
import org.bjf.modules.core.bean.BaseEntity;

/**
 * 角色菜单关系
 *
 * @author bjf
 */
@Data
@Accessors(chain = true)
@TableName("t_sys_rolemenu")
public class SysRoleMenu extends BaseEntity {

    @TableId
    private Long id;

    private Long sysRoleId;
    private Long sysMenuId;
    /**
     * 冗余过来的菜单所属应用id
     */
    private Long sysAppId;
}
