package org.bjf.modules.sys.bean;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;
import org.bjf.modules.core.bean.BaseEntity;

/**
 * @author bjf
 */
@Data
@Accessors(chain = true)
@TableName("t_sys_dict_data")
public class SysDictData extends BaseEntity {

    @TableId
    private Long id;
    /**
     * 排序,越小越前
     */
    private Integer seq;

    private String dictLabel;
    private String dictValue;
    private String dictKey;

}
