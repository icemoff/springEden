package org.bjf.modules.sys.web.vo;

import lombok.Data;
import lombok.experimental.Accessors;
import org.bjf.modules.sys.bean.AuthModuleBean;

import java.util.List;

/**
 * 权限vo
 *
 * @author icemo
 */
@Data
@Accessors(chain = true)
public class PermVO extends AuthModuleBean {

    private List<PermMethod> permMethods;

    @Data
    @Accessors(chain = true)
    public static class PermMethod {
        private String methCode;
        private String methodDesc;
        /**
         * 是否已分配 1-是 0-否
         */
        private int assigned;
    }
}
