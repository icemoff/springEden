package org.bjf.modules.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.bjf.modules.sys.bean.SysUser;

public interface SysUserMapper extends BaseMapper<SysUser> {

}
