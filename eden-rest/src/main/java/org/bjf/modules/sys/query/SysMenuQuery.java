package org.bjf.modules.sys.query;

import lombok.Data;
import lombok.experimental.Accessors;
import org.bjf.modules.core.bean.Query;

/**
 * @author bjf
 */
@Data
@Accessors(chain = true)
public class SysMenuQuery extends Query {

    private Long pid;
    private String menuName;
    private Long sysAppId;
}
