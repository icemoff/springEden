package org.bjf.modules.sys.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * CRUD权限方法归类
 *
 * @author bjf
 */
public enum AuthMethod {

    CREATE("add", "add,save,create,insert", "新增"),
    READ("read", "get,list,query,search,check,detail", "读取"),
    UPDATE("update", "update,edit,modify,do", "修改"),
    DELETE("delete", "del,remove", "删除");

    /**
     * 权限编码
     */
    private String code;
    /**
     * 匹配的方法
     */
    private String matchMethod;

    private String desc;

    AuthMethod(String code, String matchMethod, String desc) {
        this.code = code;
        this.matchMethod = matchMethod;
        this.desc = desc;
    }

    /**
     * 根据方法名判断类型
     */
    public static AuthMethod match(String methodName) {
        for (AuthMethod method : AuthMethod.values()) {
            String supportMethod = method.getMatchMethod();
            String[] arry = StringUtils.split(supportMethod, ",");
            for (String support : arry) {
                if (methodName.startsWith(support)) {
                    return method;
                }
            }
        }
        // 不匹配，则定义为update权限
        return AuthMethod.UPDATE;
    }

    public String getCode() {
        return code;
    }

    public String getMatchMethod() {
        return matchMethod;
    }

    public String getDesc() {
        return desc;
    }
}
