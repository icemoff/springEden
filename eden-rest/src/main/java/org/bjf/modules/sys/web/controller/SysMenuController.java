package org.bjf.modules.sys.web.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import org.bjf.modules.core.web.core.PageVO;
import org.bjf.modules.core.web.core.ThreadContext;
import org.bjf.modules.sys.annotation.AuthModule;
import org.bjf.modules.sys.bean.SysMenu;
import org.bjf.modules.sys.query.SysMenuQuery;
import org.bjf.modules.sys.service.SysMenuService;
import org.bjf.modules.sys.service.SysRoleMenuService;
import org.bjf.modules.sys.web.vo.MenuVO;
import org.bjf.security.satoken.SaTokenAdminUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("admin")
@AuthModule(code = "menu", desc = "菜单管理")
public class SysMenuController {

    @Autowired
    private SysMenuService menuService;

    @Autowired
    private SysRoleMenuService roleMenuService;

    @RequestMapping("memnu/detail")
    public SysMenu detail(@RequestParam long id) {
        return menuService.get(id);
    }

    @RequestMapping("memnu/list")
    public List<SysMenu> list(SysMenuQuery query) {
        return menuService.list(query);
    }

    @RequestMapping("memnu/listPage")
    public PageVO<SysMenu> listPage(SysMenuQuery query) {
        return menuService.listPage(query);
    }

    @RequestMapping("memnu/listAll")
    public List<SysMenu> listAll(SysMenuQuery query) {
        return menuService.listAll(query);
    }


    @RequestMapping("memnu/add")
    @SaCheckPermission(value = "menu-add",orRole = "super-admin",type = SaTokenAdminUtil.TYPE)
    public void add(@Validated SysMenu menu) {
        menuService.add(menu);
    }

    @RequestMapping("memnu/update")
    @SaCheckPermission(value = "menu-update",orRole = "super-admin",type = SaTokenAdminUtil.TYPE)
    public void update(@Validated SysMenu menu) {
        menuService.updateById(menu);
    }

    @RequestMapping("memnu/delete")
    @SaCheckPermission(value = "menu-delete",orRole = "super-admin",type = SaTokenAdminUtil.TYPE)
    public void delete(@RequestParam long id) {
        menuService.deleteById(id);
    }

    /**
     * 查询用户的所有菜单
     */
    @RequestMapping("menu/listByLoginUser")
    public List<SysMenu> listByLoginUser() {
        long sysUserId = ThreadContext.getLoginInfo().getUserId();
        return roleMenuService.listByUser(sysUserId);
    }

    /**
     * 查询角色菜单列表(返回所有菜单，标记是否有分配)
     */
    @RequestMapping("menu/listByRole")
    public List<MenuVO> listByRole(@RequestParam Long roleId) {
        //===1.查询所有菜单
        List<SysMenu> allMenu = menuService.listAll(new SysMenuQuery());

        //===2.查询角色菜单
        Map<Long, Integer> roleMenuMap = roleMenuService.listMenu(roleId)
                .stream()
                .collect(Collectors.toMap(SysMenu::getId, e -> 1));

        //===3.设置菜单是否分配给角色的标记
        List<MenuVO> list = new ArrayList<>();
        for (SysMenu menu : allMenu) {
            MenuVO vo = new MenuVO();
            BeanUtils.copyProperties(menu, vo);
            if (roleMenuMap.containsKey(menu.getId())) {
                vo.setAssigned(1);
            }
            list.add(vo);
        }

        return list;
    }
}
