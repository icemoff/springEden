package org.bjf.modules.sys.query;

import lombok.Data;
import lombok.experimental.Accessors;
import org.bjf.modules.core.bean.Query;

import java.util.List;

/**
 * @author bjf
 */
@Data
@Accessors(chain = true)
public class SysRolePermQuery extends Query {

    /**
     * 角色ID
     */
    private Long sysRoleId;
    /**
     * 模块编码
     */
    private String moduleCode;
    /**
     * 权限编码
     */
    private String permCode;
    /**
     * 角色ID，IN查询
     */
    private List<Long> roleIdList;

}
