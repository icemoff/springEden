package org.bjf.modules.sys.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 权限模块声明
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface AuthModule {


    /**
     * 模板编码,可重复，重复时认为是同一个模块，只是取其中一个desc属性
     */
    String code();

    /**
     * 模块描述
     */
    String desc();
}
