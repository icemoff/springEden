package org.bjf.modules.sys.query;

import lombok.Data;
import lombok.experimental.Accessors;
import org.bjf.modules.core.bean.Query;

import java.util.List;

@Data
@Accessors(chain = true)
public class SysRoleQuery extends Query {

    /**
     * 角色编码，不能重复
     */
    private String code;
    private String roleName;
    private List<Long> roleIdList;
}
