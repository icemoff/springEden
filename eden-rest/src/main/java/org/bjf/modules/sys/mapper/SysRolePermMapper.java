package org.bjf.modules.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.bjf.modules.sys.bean.SysRolePerm;

public interface SysRolePermMapper extends BaseMapper<SysRolePerm> {

}
