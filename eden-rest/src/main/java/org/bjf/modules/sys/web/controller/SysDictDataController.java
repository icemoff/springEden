package org.bjf.modules.sys.web.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import org.bjf.modules.core.web.core.PageVO;
import org.bjf.modules.sys.annotation.AuthModule;
import org.bjf.modules.sys.bean.SysDictData;
import org.bjf.modules.sys.query.SysDictDataQuery;
import org.bjf.modules.sys.service.SysDictDataService;
import org.bjf.security.satoken.SaTokenAdminUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("admin")
@AuthModule(code = "sysdict", desc = "字典管理")
public class SysDictDataController {

    @Autowired
    private SysDictDataService dictDataService;

    @RequestMapping("sysdictdata/detail")
    public SysDictData detail(@RequestParam long id) {
        return dictDataService.get(id);
    }

    @RequestMapping("sysdictdata/list")
    public List<SysDictData> list(SysDictDataQuery query) {
        return dictDataService.list(query);
    }

    @RequestMapping("sysdictdata/listPage")
    public PageVO<SysDictData> listPage(SysDictDataQuery query) {
        return dictDataService.listPage(query);
    }

    @RequestMapping("sysdictdata/listAll")
    public List<SysDictData> listAll(SysDictDataQuery query) {
        return dictDataService.listAll(query);
    }


    @RequestMapping("sysdictdata/add")
    @SaCheckPermission(value = "sysdict-add", orRole = "super-admin", type = SaTokenAdminUtil.TYPE)
    public void add(@Validated SysDictData dictData) {
        dictDataService.add(dictData);
    }

    @RequestMapping("sysdictdata/update")
    @SaCheckPermission(value = "sysdict-update", orRole = "super-admin", type = SaTokenAdminUtil.TYPE)
    public void update(@Validated SysDictData dictData) {
        dictDataService.updateById(dictData);
    }

    @RequestMapping("sysdictdata/delete")
    @SaCheckPermission(value = "sysdict-delete", orRole = "super-admin", type = SaTokenAdminUtil.TYPE)
    public void delete(@RequestParam long id) {
        dictDataService.deleteById(id);
    }
}
