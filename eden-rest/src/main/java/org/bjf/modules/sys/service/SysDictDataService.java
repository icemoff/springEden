package org.bjf.modules.sys.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.bjf.modules.core.service.BaseService;
import org.bjf.modules.sys.bean.SysDictData;
import org.bjf.modules.sys.mapper.SysDictDataMapper;
import org.bjf.modules.sys.query.SysDictDataQuery;
import org.springframework.stereotype.Service;

/**
 * @author bjf
 * @date 2017/11/27
 */
@Service
@Slf4j
public class SysDictDataService extends BaseService<SysDictDataMapper, SysDictData, SysDictDataQuery> {


    @Override
    protected LambdaQueryWrapper<SysDictData> buildQuery(SysDictDataQuery q) {
        if (q == null) {
            return null;
        }
        LambdaQueryWrapper<SysDictData> qw = new LambdaQueryWrapper<>();
        qw.eq(q.getDictKey() != null, SysDictData::getDictKey, q.getDictKey());
        return qw;
    }

}
