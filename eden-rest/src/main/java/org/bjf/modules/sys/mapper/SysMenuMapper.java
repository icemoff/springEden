package org.bjf.modules.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.bjf.modules.sys.bean.SysMenu;

/**
 * @author bjf
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

}
