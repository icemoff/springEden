package org.bjf.modules.sys.web.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import org.bjf.modules.core.web.core.PageVO;
import org.bjf.modules.sys.annotation.AuthModule;
import org.bjf.modules.sys.bean.SysApp;
import org.bjf.modules.sys.query.SysAppQuery;
import org.bjf.modules.sys.service.SysAppService;
import org.bjf.security.satoken.SaTokenAdminUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("admin")
@AuthModule(code = "sysapp", desc = "系统应用管理")
public class SysAppController {

    @Autowired
    private SysAppService appService;

    @RequestMapping("sysapp/detail")
    public SysApp detail(@RequestParam long id) {
        return appService.get(id);
    }

    @RequestMapping("sysapp/list")
    public List<SysApp> list(SysAppQuery query) {
        return appService.list(query);
    }

    @RequestMapping("sysapp/listPage")
    public PageVO<SysApp> listPage(SysAppQuery query) {
        return appService.listPage(query);
    }

    @RequestMapping("sysapp/listAll")
    public List<SysApp> listAll(SysAppQuery query) {
        return appService.listAll(query);
    }


    @RequestMapping("sysapp/add")
    @SaCheckPermission(value = "sysapp-add", orRole = "super-admin", type = SaTokenAdminUtil.TYPE)
    public void add(@Validated SysApp sysApp) {
        appService.add(sysApp);
    }

    @RequestMapping("sysapp/update")
    @SaCheckPermission(value = "sysapp-update", orRole = "super-admin", type = SaTokenAdminUtil.TYPE)
    public void update(@Validated SysApp sysApp) {
        appService.updateById(sysApp);
    }

    @RequestMapping("sysapp/delete")
    @SaCheckPermission(value = "sysapp-delete", orRole = "super-admin", type = SaTokenAdminUtil.TYPE)
    public void delete(@RequestParam long id) {
        appService.deleteById(id);
    }
}
