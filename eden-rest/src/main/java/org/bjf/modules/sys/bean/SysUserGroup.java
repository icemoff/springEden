package org.bjf.modules.sys.bean;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;
import org.bjf.modules.core.bean.BaseEntity;

/**
 * 用户组
 *
 * @author bjf
 */
@Data
@Accessors(chain = true)
@TableName("t_sys_usergroup")
public class SysUserGroup extends BaseEntity {

    @TableId
    private Long id;

    /**
     * 分组名称
     */
    private String name;


}
