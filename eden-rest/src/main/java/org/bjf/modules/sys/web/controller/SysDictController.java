package org.bjf.modules.sys.web.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import org.bjf.modules.core.web.core.PageVO;
import org.bjf.modules.sys.annotation.AuthModule;
import org.bjf.modules.sys.bean.SysDict;
import org.bjf.modules.sys.query.SysDictQuery;
import org.bjf.modules.sys.service.SysDictService;
import org.bjf.security.satoken.SaTokenAdminUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("admin")
@AuthModule(code = "sysdict", desc = "字典管理")
public class SysDictController {

    @Autowired
    private SysDictService dictService;

    @RequestMapping("sysdict/detail")
    public SysDict detail(@RequestParam long id) {
        return dictService.get(id);
    }

    @RequestMapping("sysdict/list")
    public List<SysDict> list(SysDictQuery query) {
        return dictService.list(query);
    }

    @RequestMapping("sysdict/listPage")
    public PageVO<SysDict> listPage(SysDictQuery query) {
        return dictService.listPage(query);
    }

    @RequestMapping("sysdict/listAll")
    public List<SysDict> listAll(SysDictQuery query) {
        return dictService.listAll(query);
    }


    @RequestMapping("sysdict/add")
    @SaCheckPermission(value = "sysdict-add", orRole = "super-admin", type = SaTokenAdminUtil.TYPE)
    public void add(@Validated SysDict dict) {
        dictService.add(dict);
    }

    @RequestMapping("sysdict/update")
    @SaCheckPermission(value = "sysdict-update", orRole = "super-admin", type = SaTokenAdminUtil.TYPE)
    public void update(@Validated SysDict dict) {
        dictService.updateById(dict);
    }

    @RequestMapping("sysdict/delete")
    @SaCheckPermission(value = "sysdict-delete", orRole = "super-admin", type = SaTokenAdminUtil.TYPE)
    public void delete(@RequestParam long id) {
        dictService.deleteById(id);
    }
}
