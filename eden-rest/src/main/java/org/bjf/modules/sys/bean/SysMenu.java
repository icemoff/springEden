package org.bjf.modules.sys.bean;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;
import org.bjf.modules.core.bean.BaseEntity;

/**
 * @author bjf
 */
@Data
@Accessors(chain = true)
@TableName("t_sys_menu")
public class SysMenu extends BaseEntity {

    @TableId
    private Long id;
    private Long pid;
    /**
     * 所属应用id,默认应用
     */
    private Long sysAppId = 0L;
    private String menuName;
    private String menuUrl;
    /**
     * 菜单图标
     */
    private String icon;
    /**
     * 是否显示 1-显示 0-不显示
     */
    private Integer display;
    /**
     * 排序,越小越前
     */
    private Integer seq;

}
