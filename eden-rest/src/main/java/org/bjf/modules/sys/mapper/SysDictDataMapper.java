package org.bjf.modules.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.bjf.modules.sys.bean.SysDictData;

/**
 * @author bjf
 */
public interface SysDictDataMapper extends BaseMapper<SysDictData> {

}
