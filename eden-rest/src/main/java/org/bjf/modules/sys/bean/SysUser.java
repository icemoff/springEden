package org.bjf.modules.sys.bean;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;
import org.bjf.modules.core.bean.BaseEntity;

/**
 * @author bjf
 */
@Data
@Accessors(chain = true)
@TableName("t_sys_user")
public class SysUser extends BaseEntity {

    @TableId
    private Long id;

    private String username;
    private String password;
    private String nickName;
    private String phone;
}
