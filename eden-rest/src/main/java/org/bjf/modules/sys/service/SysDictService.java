package org.bjf.modules.sys.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.bjf.modules.core.service.BaseService;
import org.bjf.modules.sys.bean.SysDict;
import org.bjf.modules.sys.mapper.SysDictMapper;
import org.bjf.modules.sys.query.SysDictQuery;
import org.springframework.stereotype.Service;

/**
 * @author bjf
 * @date 2017/11/27
 */
@Service
@Slf4j
public class SysDictService extends BaseService<SysDictMapper, SysDict, SysDictQuery> {


    @Override
    protected LambdaQueryWrapper<SysDict> buildQuery(SysDictQuery q) {
        if (q == null) {
            return null;
        }
        LambdaQueryWrapper<SysDict> qw = new LambdaQueryWrapper<>();
        qw.eq(q.getDictKey() != null, SysDict::getDictKey, q.getDictKey());
        qw.eq(q.getStatus() != null, SysDict::getStatus, q.getStatus());
        return qw;
    }

}
