package org.bjf.modules.sys.bean;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;
import org.bjf.modules.core.bean.BaseEntity;

/**
 * 系统应用
 *
 * @author bjf
 */
@Data
@Accessors(chain = true)
@TableName("t_sys_app")
public class SysApp extends BaseEntity {

    @TableId
    private Long id;
    private String name;
    /**
     * 排序,越小越前
     */
    private Integer seq;

}
