package org.bjf.modules.monitor.bean;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 服务器信息
 */
@Data
@Accessors(chain = true)
public class ServerInfo {

    /**
     * CPU相关信息
     */
    private Cpu cpu;

    /**
     * 內存相关信息
     */
    private Mem mem;

    /**
     * JVM相关信息
     */
    private Jvm jvm;

    /**
     * 服务器相关信息
     */
    private Sys sys;

    /**
     * 磁盘相关信息
     */
    private List<SysFile> sysFiles;

}