package org.bjf.modules.websocket.bean;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * websocket 响应
 *
 * @author bjf
 */
@Data
@Accessors(chain = true)
public class SocketResp {

    private Integer type;
    private String content;
}
