package org.bjf.modules.websocket.controller;

import org.bjf.modules.websocket.bean.SocketReq;
import org.bjf.modules.websocket.bean.SocketResp;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class WebSocketController {

    @MessageMapping("/hello")
    @SendTo("/topic/chat")
    public SocketResp say(SocketReq req) throws InterruptedException {
        System.out.println("=====================");
        Thread.sleep(3000);
        SocketResp resp = new SocketResp();
        resp.setContent("xxxxxxxxxxxxoooooo");
        return resp;
    }
}
