package org.bjf.modules.websocket.bean;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * websocket 请求
 *
 * @author bjf
 */
@Data
@Accessors(chain = true)
public class SocketReq {

    private String msg;

}
