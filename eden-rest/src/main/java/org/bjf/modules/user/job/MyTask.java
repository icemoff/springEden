package org.bjf.modules.user.job;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
@Slf4j
public class MyTask {

    @Scheduled(cron = "${cron.test}")
    public void test() throws InterruptedException {

        log.info("tt1 start");
        TimeUnit.SECONDS.sleep(20);
        log.info("tt1 end");
    }

    public void test1() throws InterruptedException {
        log.info("tt3 start");
        TimeUnit.SECONDS.sleep(5);
        log.info("tt3 end");
    }

}
