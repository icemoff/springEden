package org.bjf.modules.user.service;

import org.apache.shardingsphere.infra.hint.HintManager;
import org.bjf.modules.shardDemo.mapper.ShardDemoMapper;
import org.bjf.modules.user.bean.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShardDemoService {

    @Autowired
    private ShardDemoMapper shardDemoMapper;

    public User get(long id) {
        // 强制从主库查询
        HintManager hintManager = HintManager.getInstance();
        hintManager.setWriteRouteOnly();

        return shardDemoMapper.selectById(id);
    }

    public int insert(User user) {
        return shardDemoMapper.insert(user);
    }

}
