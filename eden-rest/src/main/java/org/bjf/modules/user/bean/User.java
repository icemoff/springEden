package org.bjf.modules.user.bean;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author bjf
 */
@Data
@Accessors(chain = true)
@TableName("t_user")
public class User {

    @TableId
    private Long userId;

    private String username;
    private String password;
    private String nickName;
    private String phone;

    private Date ctime;

    private Date utime;
}
