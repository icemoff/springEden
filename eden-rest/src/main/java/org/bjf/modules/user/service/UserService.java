package org.bjf.modules.user.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.bjf.modules.core.service.BaseService;
import org.bjf.modules.user.bean.User;
import org.bjf.modules.user.mapper.UserMapper;
import org.bjf.modules.user.query.UserQuery;
import org.springframework.stereotype.Service;

/**
 * @author bjf
 * @date 2017/11/27
 */
@Service
@Slf4j
public class UserService extends BaseService<UserMapper, User, UserQuery> {


    @Override
    protected LambdaQueryWrapper<User> buildQuery(UserQuery q) {
        if (q == null) {
            return null;
        }
        LambdaQueryWrapper<User> qw = new LambdaQueryWrapper<>();
        qw.eq(StringUtils.isNotBlank(q.getUsername()), User::getUsername, q.getUsername());
        qw.eq(StringUtils.isNotBlank(q.getPhone()), User::getPhone, q.getPhone());
        return qw;
    }
}
