package org.bjf.modules.user.web.controller.admin;

import org.bjf.modules.core.web.core.LoginRequired;
import org.bjf.modules.core.web.core.PageVO;
import org.bjf.modules.sys.annotation.AuthModule;
import org.bjf.modules.user.bean.User;
import org.bjf.modules.user.query.UserQuery;
import org.bjf.modules.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AuthModule(code = "user", desc = "用户管理 ")
@RequestMapping("admin")
public class AdminUserController {

    @Autowired
    private UserService userService;

    @RequestMapping("user/detail")
    public User detail(@RequestParam long userId) {
        return userService.get(userId);
    }

    @RequestMapping("user/list")
    public List<User> list(UserQuery query) {
        return userService.list(query);
    }

    @RequestMapping("user/listPage")
    public PageVO<User> listPage(UserQuery query) {
        return userService.listPage(query);
    }

    @RequestMapping("user/add")
    public void add(@Validated User user) {
        userService.add(user);
    }

    @RequestMapping("user/update")
    public void update(@Validated User user) {
        userService.updateById(user);
    }

    @RequestMapping("user/delete")
    public void delete(@RequestParam long userId) {
        userService.deleteById(userId);
    }


}
