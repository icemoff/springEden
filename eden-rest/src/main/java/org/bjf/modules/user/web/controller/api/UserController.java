package org.bjf.modules.user.web.controller.api;

import lombok.Data;
import lombok.experimental.Accessors;
import org.bjf.exception.ServiceException;
import org.bjf.modules.core.web.core.LoginInfo;
import org.bjf.modules.user.bean.User;
import org.bjf.modules.user.enums.UserMsgCode;
import org.bjf.modules.user.query.UserQuery;
import org.bjf.modules.user.service.UserService;
import org.bjf.security.PasswordUtil;
import org.bjf.security.satoken.SaTokenAdminUtil;
import org.bjf.security.satoken.SaTokenAppUtil;
import org.bjf.utils.ExceptionAssert;
import org.bjf.utils.IpUtil;
import org.bjf.utils.RedisUtil;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotBlank;

/**
 * @author bjf
 */
@RestController
@Validated
@RequestMapping("api")
public class UserController {

    @Resource
    private RedisUtil redis;

    @Resource
    private UserService userService;

    @RequestMapping("register")
    public void register(@RequestParam @NotBlank String username,
                         @RequestParam @NotBlank String password) {
        //===1、看用户名是否已存在
        UserQuery query = new UserQuery();
        query.setUsername(username);
        boolean exist = userService.exist(query);
        ExceptionAssert.isTrue(!exist, UserMsgCode.USER_HAS_EXIST);

        //===2、新增用户
        User user = new User();
        user.setUsername(username);
        user.setPassword(PasswordUtil.encode(password));
        userService.add(user);
    }

    @RequestMapping("login")
    public LoginResp login(@RequestParam @NotBlank String username,
                           @RequestParam @NotBlank String password, HttpServletRequest request) {
        //===1、验证用户名密码
        UserQuery query = new UserQuery();
        query.setUsername(username);
        User loginUser = userService.getOne(query);
        if (loginUser == null || !PasswordUtil.valid(password, loginUser.getPassword())) {
            throw new ServiceException(UserMsgCode.USER_WRONG);
        }

        //===2、登录成功，生成token并将登录用户信息写入redis
        SaTokenAdminUtil.login(loginUser.getUserId());
        String accessToken = SaTokenAdminUtil.getTokenValue();
        Long userId = loginUser.getUserId();
        String ip = IpUtil.getIp(request);
        LoginInfo loginInfo = LoginInfo
                .buildLoginInfo(userId, loginUser.getUsername(), ip);
        // 数据是会放到redis
        SaTokenAppUtil.getTokenSession().set("loginInfo", loginInfo);

        //===3、返回登录信息-accessToken
        LoginResp loginResp = new LoginResp();
        loginResp.setAccessToken(accessToken);
        return loginResp;
    }

    @Data
    @Accessors(chain = true)
    private static class LoginResp {

        private String accessToken;
    }
}
