package org.bjf.modules.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.bjf.modules.user.bean.User;

public interface UserMapper extends BaseMapper<User> {

}
