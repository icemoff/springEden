package org.bjf.modules.user.enums;

/**
 * 使用枚举有更高的可读性，在代码上约束常量泛滥
 *
 * @author bjf
 */
public enum UserRedisKey {
    TOKEN_API("live:api:accessToken", "token对应的登录用户信息"),
    TOKEN_ADMIN("live:admin:accessToken", "token对应的登录用户信息"),
    INVALID_REFRESH_TOKEN("live:user:invalidRefreshToken", "jwt invalid refreshToken");

    private String name;
    private String desc;

    UserRedisKey(String name, String desc) {
        this.name = name;
        this.desc = desc;
    }

    public String as(Object... args) {
        StringBuilder sb = new StringBuilder(name);
        for (Object arg : args) {
            sb.append(":").append(arg.toString());
        }
        return sb.toString();
    }
}
