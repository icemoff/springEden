package org.bjf.modules.user.query;

import lombok.Data;
import lombok.experimental.Accessors;
import org.bjf.modules.core.bean.Query;

@Data
@Accessors(chain = true)
public class UserQuery extends Query {

    private String username;
    private String phone;
}
