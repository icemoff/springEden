package org.bjf.modules.user.job;


import org.apache.shardingsphere.elasticjob.api.ShardingContext;
import org.apache.shardingsphere.elasticjob.simple.job.SimpleJob;

public class MyJob implements SimpleJob {

    @Override
    public void execute(ShardingContext context) {
        System.out.println(" job名称:" + context.getJobName()
                + ",分片数量:" + context.getShardingTotalCount()
                + ",当前分区:" + context.getShardingItem()
                + ",当前分区名称:" + context.getShardingParameter()
                + ",当前自定义参数:" + context.getJobParameter());
    }

}