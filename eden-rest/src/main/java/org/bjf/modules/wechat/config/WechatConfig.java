package org.bjf.modules.wechat.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * 微信配置
 * 微信公众平台测试号：https://mp.weixin.qq.com/debug/cgi-bin/sandboxinfo?action=showinfo&t=sandbox/index
 * 小程序测试号： https://mp.weixin.qq.com/wxamp/sandbox?doc=1
 */
@Configuration
public class WechatConfig {

    //公众号appid
    public static String mpAppId;

    //公众号appSecret
    public static String mpAppSecret;

    //商户号
    public static String mchId;

    //商户秘钥
    public static String mchKey;

    //商户证书路径
    public static String keyPath;

    //微信支付异步通知
    public static String notifyUrl;

    //开放平台id
    public static String openAppId;

    //开放平台秘钥
    public static String openAppSecret;


    @Value("${wechat.mp-app-id:}")
    public static void setMpAppId(String mpAppId) {
        WechatConfig.mpAppId = mpAppId;
    }

    @Value("${wechat.mp-app-secret:}")
    public static void setMpAppSecret(String mpAppSecret) {
        WechatConfig.mpAppSecret = mpAppSecret;
    }

    @Value("${wechat.mch-id:}")
    public static void setMchId(String mchId) {
        WechatConfig.mchId = mchId;
    }

    @Value("${wechat.mch-key:}")
    public static void setMchKey(String mchKey) {
        WechatConfig.mchKey = mchKey;
    }

    @Value("${wechat.mch-key-path:}")
    public static void setKeyPath(String keyPath) {
        WechatConfig.keyPath = keyPath;
    }

    @Value("${wechat.mch-notify-url:}")
    public static void setNotifyUrl(String notifyUrl) {
        WechatConfig.notifyUrl = notifyUrl;
    }

    @Value("${wechat.open-app-id:}")
    public static void setOpenAppId(String openAppId) {
        WechatConfig.openAppId = openAppId;
    }

    @Value("${wechat.open-app-secret:}")
    public static void setOpenAppSecret(String openAppSecret) {
        WechatConfig.openAppSecret = openAppSecret;
    }
}
