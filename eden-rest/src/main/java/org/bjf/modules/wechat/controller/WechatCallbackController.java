package org.bjf.modules.wechat.controller;

import org.bjf.modules.wechat.dto.WechatMpSignature;
import org.bjf.modules.wechat.util.WechatUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/wechat/callback")
public class WechatCallbackController {


    /**
     * 公众号上配置的服务器地址，校验用
     */
    @RequestMapping("mp/check")
    public String mpCheck(WechatMpSignature req) {
        if (WechatUtils.checkSignature(req.getSignature(), req.getTimestamp(), req.getNonce())) {
            return req.getEchostr();
        }
        return "校验不通过";
    }

}
