package org.bjf.modules.wechat.gateway;

import lombok.Data;

@Data
public class WxUserInfoDO extends WxBaseDO{


    private String openid;

    private String nickname;

    private Integer sex;

    private String province;
    private String city;
    private String country;
    private String headimgurl;
    /**
     * 用户特权信息，json 数组，如微信沃卡用户为 ["chinaunicom" "privilege2"]
     */
    private String unionid;
}
