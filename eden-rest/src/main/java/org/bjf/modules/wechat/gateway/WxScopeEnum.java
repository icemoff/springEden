package org.bjf.modules.wechat.gateway;


/**
 * -snsapi_base （不弹出授权页面，直接跳转，只能获取用户openid）,静默授权的，用户无感知
 * -snsapi_userinfo （弹出授权页面，可通过openid拿到昵称、性别、所在地。并且， 即使在未关注的情况下，只要用户授权，也能获取其信息 ）
 * -snsapi_login 网页应用目前仅填写
 */
public enum WxScopeEnum {
    SNSAPI_BASE("snsapi_base"),
    SNSAPI_USERINFO("snsapi_userinfo"),
    SNSAPI_LOGIN("snsapi_login");
    private String scope;

    WxScopeEnum(String scope) {
        this.scope = scope;
    }

    public String getScope() {
        return scope;
    }
}