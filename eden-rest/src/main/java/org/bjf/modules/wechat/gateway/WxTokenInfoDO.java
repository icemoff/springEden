package org.bjf.modules.wechat.gateway;

import com.alibaba.fastjson2.annotation.JSONField;
import lombok.Data;

@Data
public class WxTokenInfoDO  extends WxBaseDO{

    @JSONField(name = "access_token")
    private String accessToken;

    @JSONField(name = "expires_in")
    private Integer expiresIn;

    @JSONField(name = "refresh_token")
    private String refreshToken;

    private String openid;

    private String scope;

    private String unionid;
}
