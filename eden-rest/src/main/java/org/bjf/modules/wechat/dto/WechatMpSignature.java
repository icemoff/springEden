package org.bjf.modules.wechat.dto;

import lombok.Data;


/**
 * 微信公众号签名验证对象
 */
@Data
public class WechatMpSignature {
    private String signature;
    private String timestamp;
    private String nonce;
    private String echostr;
}