package org.bjf.modules.wechat.gateway;

import lombok.Data;

@Data
public class WxBaseDO {
    private String errcode;
    private String errmsg;

}
