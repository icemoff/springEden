package org.bjf.modules.shardDemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.bjf.modules.user.bean.User;

public interface ShardDemoMapper extends BaseMapper<User> {

}