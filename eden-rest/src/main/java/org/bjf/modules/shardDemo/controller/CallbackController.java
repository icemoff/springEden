package org.bjf.modules.shardDemo.controller;


import org.apache.commons.lang3.StringUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/callback")
public class CallbackController {


    @RequestMapping("payCallback")
    public String payCallback(@RequestParam MultiValueMap<String, String> formData) {

        Set<Map.Entry<String, List<String>>> entries = formData.entrySet();

        String signStr = entries.stream()
                .sorted(Map.Entry.comparingByKey())
                .map(Map.Entry::getValue)
                .filter(l -> l.size() == 1)
                .map(l -> l.get(0))
                .filter(StringUtils::isNotBlank)
                .collect(Collectors.joining("", "xxoo", ""));

        System.out.println("signStr = " + signStr);


        return signStr;
    }
}
