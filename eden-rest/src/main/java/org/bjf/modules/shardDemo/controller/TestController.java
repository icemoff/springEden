package org.bjf.modules.shardDemo.controller;

import com.google.common.hash.Funnels;
import lombok.extern.slf4j.Slf4j;
import org.bjf.aop.MultiPostResolver;
import org.bjf.cache.CacheNameConfig.RedisCacheEnum.Names;
import org.bjf.modules.sys.bean.SysUser;
import org.bjf.modules.user.bean.User;
import org.bjf.modules.user.service.ShardDemoService;
import org.bjf.utils.BloomFilterHelper;
import org.bjf.utils.RedisUtil;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

@RestController
@Slf4j
@RefreshScope
public class TestController {


    @Resource
    private ShardDemoService shardDemoService;
    @Resource
    private RedisUtil redis;
    @Autowired
    protected RedisTemplate redisTemplate;
    @Autowired
    RedissonClient redissonClient;

    @RequestMapping("test/user/{id}")
    public User detail(@PathVariable long id) {
        return shardDemoService.get(id);
    }

    @RequestMapping("test/log")
    public Object logger() {

        SysUser user = new SysUser().setPassword("999").setUsername("bjf");
        redis.setObj("user", user,999L);
        SysUser user1 = redis.getObj("user");
        return user1;
    }

    @RequestMapping("test/formData")
    @MultiPostResolver
    public void formData(User user) {
        System.out.println("user = " + user);
    }

    @Cacheable(value = Names.USER, key = "'live:test:cache:' + #cacheId", sync = true)
    @RequestMapping("test/cache")
    public Object testCache(String cacheId) {
        log.info("from db");
        User user = new User();
        user.setUsername(cacheId);
        return user;
    }

    @RequestMapping("test/up")
    public Object up(@RequestParam("img") MultipartFile file) {

        log.info("xxxxxxxxxxx");

        return "ok";
    }


    // ======BloomFilter

    private static BloomFilterHelper<Integer> bloomFilterHelper;

    static {
        bloomFilterHelper = new BloomFilterHelper<>(Funnels.integerFunnel(), 10000);
    }

    @Value("${hello:xxx}")
    private String hello;

    @RequestMapping("test/nacos")
    public void nacos() {
        System.out.println("hello = " + hello);
    }

}
