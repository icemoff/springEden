package org.bjf.modules.shardDemo.bean;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
@TableName("user")
public class User implements Serializable {

    private Long id;

    private String phone;

    private String openId;

    private String passwd;

    private String source;

    private String nick;

    private Integer sex;

    private String sign;

    private String photo;

    private Integer identity;

    private Integer status;

    private String forbiddenReason;

    private Date ctime;

    private Date utime;

}