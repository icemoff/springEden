package org.bjf.modules.core.web.core;

/**
 * Created by lenovo on 2017/8/8.
 */
public class ThreadContext {

    private static final ThreadLocal<LoginInfo> LOGIN_INFO_THREAD_LOCAL = new ThreadLocal<>();

    public static LoginInfo getLoginInfo() {
        return LOGIN_INFO_THREAD_LOCAL.get();
    }

    public static void setLoginInfo(LoginInfo loginInfo) {
        LOGIN_INFO_THREAD_LOCAL.set(loginInfo);
    }

    public static void clear() {
        LOGIN_INFO_THREAD_LOCAL.remove();
    }

}
