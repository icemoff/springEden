package org.bjf.modules.core.web.core;

import lombok.Data;
import lombok.experimental.Accessors;
import org.bjf.utils.TraceUtil;

/**
 * 统一返回包装 Created by lenovo on 2017/7/29.
 */
@Data
@Accessors(chain = true)
public class LiveResp<T> {

    private int code = 200;
    private String msg = "";
    private T data;
    private String traceId = TraceUtil.getTraceId();
    /**
     * 响应时间戳
     */
    private Long timestamp = System.currentTimeMillis();

    public LiveResp() {
    }

    public LiveResp(T data) {
        this.data = data;
    }

    public LiveResp(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
