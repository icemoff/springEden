package org.bjf.modules.core.web.core;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by lenovo on 2017/8/7.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface LoginRequired {

    LoginType loginType() default LoginType.MEMBER;

    enum LoginType {
        /**
         * 权限类型，现在只有MEMBER-普通用户
         */
        MEMBER
    }

}
