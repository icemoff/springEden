package org.bjf.modules.core.bean;

import lombok.Data;

/**
 * @author binjinfeng
 */

@Data
public class Query {

    private int page = 1;
    private int pageSize = 20;
}