package org.bjf.modules.core.web.core;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author bjf
 */
@Data
@Accessors(chain = true)
public class LoginInfo {


    /**
     * 用户id
     */
    private long userId;

    /**
     * 登录名称
     */
    private String loginName;

    /**
     * 登录时间
     */
    private Date loginTime;
    /**
     * 最后访问时间
     */
    private Date lastTime;

    /**
     * 登录ip
     */
    private String loginIp;


    /**
     * 构建登录的用户信息
     */
    public static LoginInfo buildLoginInfo(long userId, String loginName,
                                           String loginIp) {
        Date now = new Date();

        return new LoginInfo()
                .setUserId(userId)
                .setLoginName(loginName)
                .setLoginIp(loginIp)
                .setLoginTime(now);
    }

}
