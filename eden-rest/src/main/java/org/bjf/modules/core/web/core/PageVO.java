package org.bjf.modules.core.web.core;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Collection;

@Data
@Accessors(chain = true)
public class PageVO<E> {

    /**
     * 总条数
     */
    private long total;

    /**
     * 查询结果
     */
    private Collection<E> records;

    private Object extraData;

    /**
     * 当前页
     */
    private long page;

    /**
     * 总页数
     */
    private long totalPage;

    /**
     * 每页显示条数
     */
    private long pageSize;

    public PageVO() {
    }


    public PageVO(Collection<E> records, Page page) {
        this.records = records;
        this.total = page.getTotal();
        this.totalPage = page.getPages();
        this.page = page.getCurrent();
        this.pageSize = page.getSize();
    }

    public PageVO(Collection<E> records, PageVO pageVO) {
        this.records = records;
        this.total = pageVO.getTotal();
        this.totalPage = pageVO.getTotalPage();
        this.page = pageVO.getPage();
        this.pageSize = pageVO.getPageSize();
    }

}