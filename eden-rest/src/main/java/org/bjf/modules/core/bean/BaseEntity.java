package org.bjf.modules.core.bean;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class BaseEntity {

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date ctime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date utime;
}
