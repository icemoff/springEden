package org.bjf.modules.core.web.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * 使用logback邮件，暂无用处 Created by binjinfeng
 */
//@Component
@Slf4j
public class EmailHandler {

    @Autowired
    private JavaMailSender mailSender;

    @Value("${mail.username}")
    private String from;
    private String[] to = {"binjinfeng@test.com"};


    @Async
    public void asyncMail(String subject, String text) {
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setFrom(from);
            message.setTo(to);
            message.setSubject(subject);
            message.setText(text);
            mailSender.send(message);
        } catch (MailException e) {
            log.error("邮件发送失败", e);
        }
    }
}
