package org.bjf.modules.wechatpay.query;

import com.alibaba.fastjson2.annotation.JSONField;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 关闭订单
 *
 * @author bjf
 * @see https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=9_3&index=5
 */
@Data
@Accessors(chain = true)
public class CloseOrder extends WechatRequest {

    /**
     * 商户订单号
     */
    @JSONField(name = "out_trade_no")
    private String outTradeNo;

    public CloseOrder() {
        super("https://api.mch.weixin.qq.com/pay/closeorder");
    }

    @Data
    @Accessors(chain = true)
    public static class CloseOrderResp extends WechatResponse {

        //============以下字段在return_code为SUCCESS的时候有返回
        private String appid;
        private String mch_id;
        private String nonce_str;
        private String sign;
        /**
         * 业务结果
         */
        private String result_code;
        private String result_msg;
        private String err_code;
        private String err_code_des;
    }
}
