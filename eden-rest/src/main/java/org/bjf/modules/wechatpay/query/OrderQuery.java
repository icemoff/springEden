package org.bjf.modules.wechatpay.query;

import com.alibaba.fastjson2.annotation.JSONField;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 查询订单
 *
 * @author bjf
 * @see https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=9_2&index=4
 */
@Data
@Accessors(chain = true)
public class OrderQuery extends WechatRequest {

    /**
     * 商户订单号
     */
    @JSONField(name = "out_trade_no")
    private String outTradeNo;

    public OrderQuery() {
        super("https://api.mch.weixin.qq.com/pay/orderquery");
    }

    @Data
    @Accessors(chain = true)
    public static class OrderQueryResp extends WechatResponse {

        //============以下字段在return_code为SUCCESS的时候有返回
        private String appid;
        private String mch_id;
        private String nonce_str;
        private String sign;
        /**
         * 业务结果
         */
        private String result_code;
        private String err_code;
        private String err_code_des;

        //============以下字段在return_code 和result_code都为SUCCESS的时候有返回
        private String device_info;
        private String openid;
        private String is_subscribe;
        private String trade_type;
        private String trade_state;
        private String bank_type;
        private String total_fee;
        private String fee_type;
        private String cash_fee;
        private String cash_fee_type;
        private String settlement_total_fee;
        private String coupon_fee;
        private String coupon_count;
        private String coupon_id_$n;
        private String coupon_type_$n;
        private String coupon_fee_$n;
        private String transaction_id;
        private String out_trade_no;
        private String attach;
        private String time_end;
        private String trade_state_desc;
    }


}
