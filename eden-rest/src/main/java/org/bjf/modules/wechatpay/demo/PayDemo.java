package org.bjf.modules.wechatpay.demo;

import org.bjf.modules.wechatpay.core.WechatPay;
import org.bjf.modules.wechatpay.query.OrderQuery;
import org.bjf.modules.wechatpay.query.OrderQuery.OrderQueryResp;


/**
 * 微信支付接口封装,仅需定义请求响应的实体类即可
 *
 * @author bjf
 */
public class PayDemo {


    /**
     * 订单查询
     */
    public void testOrderQuery() {
        OrderQuery orderQuery = new OrderQuery();
        orderQuery.setOutTradeNo("tttttttt");
        OrderQueryResp resp = WechatPay.getInstance().execute(orderQuery, OrderQueryResp.class);

    }

}
