package org.bjf.modules.wechatpay.query;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 统一下单
 *
 * @author bjf
 * @see https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=9_1
 */
@Data
@Accessors(chain = true)
public class UnifiedOrder extends WechatRequest {

    /**
     * 终端设备号(门店号或收银设备ID)，默认请传"WEB"
     */
    private String device_info = "WEB";
    /**
     * 商品描述,APP——需传入应用市场上的APP名字-实际商品名称，天天爱消除-游戏充值。
     */
    private String body;
    /**
     * 商品详情 ,非必填
     */
    private String detail;
    /**
     * 附加数据,非必填
     */
    private String attach;
    /**
     * 商户订单号
     */
    private String out_trade_no;
    /**
     * 货币类型,默认人民币：CNY
     */
    private String fee_type = "CNY";
    /**
     * 订单总金额，单位为分
     */
    private String total_fee;
    /**
     * 终端IP
     */
    private String spbill_create_ip;
    /**
     * 订单生成时间，格式为yyyyMMddHHmmss,非必填
     */
    private String time_start;
    /**
     * 订单失效时间，格式为yyyyMMddHHmmss，如2009年12月27日9点10分10秒表示为20091227091010。订单失效时间是针对订单号而言的，由于在请求支付的时候有一个必传参数prepay_id只有两小时的有效期，所以在重入时间超过2小时的时候需要重新请求下单接口获取新的prepay_id。其他详见时间规则
     * <p>
     * 建议：最短失效时间间隔大于1分钟 ,非必填
     */
    private String time_expire;
    /**
     * 订单优惠标记,非必填
     */
    private String goods_tag;
    /**
     * notify_url
     */
    private String notify_url;
    /**
     * 支付类型
     */
    private String trade_type = "APP";
    /**
     * 指定支付方式,非必填 ,no_credit--指定不能使用信用卡支付
     */
    private String limit_pay;
    /**
     * 场景信息,非必填
     */
    private String scene_info;

    public UnifiedOrder() {
        super("https://api.mch.weixin.qq.com/pay/unifiedorder");
    }

    @Data
    @Accessors(chain = true)
    public static class UnifiedOrderResp extends WechatResponse {

        //============以下字段在return_code为SUCCESS的时候有返回
        private String appid;
        private String mch_id;
        private String device_info;
        private String nonce_str;
        private String sign;
        /**
         * 业务结果
         */
        private String result_code;
        private String err_code;
        private String err_code_des;

        //============以下字段在return_code 和result_code都为SUCCESS的时候有返回
        private String trade_type;
        private String prepay_id;
    }


}
