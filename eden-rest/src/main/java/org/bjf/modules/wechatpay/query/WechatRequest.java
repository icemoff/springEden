package org.bjf.modules.wechatpay.query;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.experimental.Accessors;
import org.bjf.modules.wechatpay.constants.PayConstant;

import java.util.UUID;

/**
 * @author bjf
 */
@Data
@Accessors(chain = true)
public class WechatRequest {

    /**
     * 接口URL
     */
    @JsonIgnore
    private transient String url;
    /**
     * 应用ID
     */
    private String appid = PayConstant.APPID;
    /**
     * 商户号
     */
    private String mch_id = PayConstant.MCHID;

    /**
     * 随机字符串,不长于32位
     */
    private String nonce_str = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
    /**
     * 签名类型，目前支持HMAC-SHA256和MD5，默认为MD5
     */
    private String sign_type = "MD5";
    /**
     * 签名
     */
    private String sign;


    public WechatRequest(String url) {
        this.url = url;
    }

}
