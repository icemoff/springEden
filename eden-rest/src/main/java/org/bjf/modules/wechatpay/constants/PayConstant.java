package org.bjf.modules.wechatpay.constants;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PayConstant {

    /**
     * 微信appid
     */
    public static String APPID = "appdddddid";
    /**
     * 微信商户号
     */
    public static String MCHID = "mmmchId";
    /**
     * 微信商户号
     */
    public static String SECRETKEY = "sssssskey";

    @Value("${appid:}")
    public void setAppId(String appId) {
        APPID = appId;
    }

    @Value("${mchId:}")
    public void setMchId(String mchId) {
        MCHID = mchId;
    }

    @Value("${secretKey:}")
    public void setSecretKey(String SECRETKEY) {
        SECRETKEY = SECRETKEY;
    }
}
