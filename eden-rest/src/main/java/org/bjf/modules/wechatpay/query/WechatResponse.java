package org.bjf.modules.wechatpay.query;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author bjf
 */
@Data
@Accessors(chain = true)
public class WechatResponse {

    private String return_code;
    private String return_msg;

}
