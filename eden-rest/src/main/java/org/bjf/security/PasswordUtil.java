package org.bjf.security;

import org.springframework.security.crypto.bcrypt.BCrypt;

public class PasswordUtil {

    /**
     * BCrypt 算法加密，安全，但没MD5快
     */
    public static String encode(String password) {
        if (password == null) {
            throw new IllegalArgumentException("密码不能为空");
        } else {
            return BCrypt.hashpw(password.toString(), BCrypt.gensalt());
        }
    }

    public static boolean valid(String password, String encodedPassword) {
        return BCrypt.checkpw(password, encodedPassword);
    }
}
