package org.bjf.exception;


public interface MsgCode {

    int getCode();

    String getMessage();

}
