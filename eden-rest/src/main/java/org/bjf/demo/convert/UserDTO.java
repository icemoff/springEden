package org.bjf.demo.convert;

import com.google.common.base.Converter;
import lombok.Data;
import lombok.experimental.Accessors;
import org.bjf.modules.sys.bean.SysUser;
import org.springframework.beans.BeanUtils;

/**
 * @author bjf on 2021/10/4.
 * @version 1.022222
 */
@Data
@Accessors(chain = true)
public class UserDTO {
    private Long id;
    private String nickname;
    private String phone;
    private String address;

    public SysUser convertToUser() {
        return new UserDTOConvert().convert(this);
    }

    public UserDTO convertForm(SysUser user) {
        return new UserDTOConvert().reverse().convert(user);
    }

    private static class UserDTOConvert extends Converter<UserDTO, SysUser> {

        @Override
        protected SysUser doForward(UserDTO userDTO) {
            SysUser user = new SysUser();
            BeanUtils.copyProperties(userDTO, user);
            return user;
        }

        @Override
        protected UserDTO doBackward(SysUser sysUser) {
            throw new AssertionError("不支持逆向转化方法!");
        }
    }


    public static void main(String[] args) {
        UserDTO userDTO = new UserDTO().setPhone("1244242342");
        SysUser user = userDTO.convertToUser();
        System.out.println("user = " + user);
    }
}
