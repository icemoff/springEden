package org.bjf.demo.enableclz;

import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;

@ConditionalOnWebApplication
public class LogFilterWebConfig {

    @Bean
    public LogFilter logFilter() {
        return new LogFilter();
    }
}