package org.bjf.demo.enableclz;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 在application.yml里配置：demo.enable: true生效
 */
@ConditionalOnProperty(prefix = "demo", name = "enable", havingValue = "true", matchIfMissing = false)
@Configuration
public class PropConfig {

    @Bean
    public LogFilter logFilter() {
        return new LogFilter();
    }
}