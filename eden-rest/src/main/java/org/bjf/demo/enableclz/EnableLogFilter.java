package org.bjf.demo.enableclz;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 在Application启动类上加@EnableLogFilter生效
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(LogFilterWebConfig.class)
public @interface EnableLogFilter {

}