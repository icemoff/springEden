package org.bjf.demo.nestedbiztemplate;

import org.bjf.modules.user.bean.User;

/**
 * 业务模板 方法
 *
 * @author bjf on 2021/5/6.
 * @version 1.022222
 */
public class UserRegisterService {
    abstract class RegisterProcess {
        public final void doService(User user) {
            //====1.校验用户信息
            validInfo(user);
            //====2.DB新增用户
            addUser(user);
            //====3.生成token并缓存用户信息
            genToken(user);
        }

        public void validInfo(User user) {
        }

        protected abstract void addUser(User user);

        protected abstract void genToken(User user);
    }


    public void register(User user) {
        new RegisterProcess() {
            @Override
            protected void addUser(User user) {
            }

            @Override
            protected void genToken(User user) {

            }
        }.doService(user);
    }
}
