package org.bjf.demo.msgcenter;

import org.bjf.utils.SpringUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author bjf on 2021/5/6.
 * @version 1.022222
 */
public enum MessageTypeEnum {

    /**
     * 邮件
     */
    EMAIL("邮件", EmailMessageService.class),
    /**
     * 短信
     */
    SMS("短信", SmsMessageService.class),
    /**
     * 微信
     */
    WECHAT("微信", WechatMessageService.class);
    private static Map<String, MessageBizService> SERVICE_CACHE = new ConcurrentHashMap<>(16);
    private static Map<String, MessageTypeEnum> ENUM_MAP = new HashMap<>(16);

    static {
        for (MessageTypeEnum item : MessageTypeEnum.values()) {
            ENUM_MAP.put(item.name(), item);
        }
    }

    private String desc;
    private Class<? extends MessageBizService> serviceType;

    MessageTypeEnum(String desc, Class<? extends MessageBizService> serviceType) {
        this.desc = desc;
        this.serviceType = serviceType;
    }

    public String getDesc() {
        return desc;
    }

    public Class<? extends MessageBizService> getServiceType() {
        return serviceType;
    }

    public MessageBizService getService() {
        return SERVICE_CACHE.computeIfAbsent(this.name(), v -> SpringUtil.getBean(this.getServiceType()));
    }

    public static MessageTypeEnum resolve(String enumName) {
        return enumName != null ? ENUM_MAP.getOrDefault(enumName.toUpperCase(), SMS) : null;
    }
}
