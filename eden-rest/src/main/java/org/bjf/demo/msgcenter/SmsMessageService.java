package org.bjf.demo.msgcenter;

import org.springframework.stereotype.Service;

/**
 * @author bjf on 2021/5/8.
 * @version 1.022222
 */
@Service
public class SmsMessageService extends MessageBizService {
    @Override
    public void send(String msg) {
        System.out.println("send sms");
    }
}
