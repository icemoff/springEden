package org.bjf.demo.msgcenter;

import org.springframework.stereotype.Service;

/**
 * @author bjf on 2021/5/8.
 * @version 1.022222
 */
@Service
public class WechatMessageService extends MessageBizService {
    @Override
    public void send(String msg) {
        System.out.println("send wechat");
    }

    @Override
    public void afterSendSuccess() {
        System.out.println("微信消息发送成功");
    }
}
