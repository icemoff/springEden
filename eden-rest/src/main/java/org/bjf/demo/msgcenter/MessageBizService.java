package org.bjf.demo.msgcenter;

/**
 * 消息服务
 *
 * @author bjf on 2021/5/6.
 * @version 1.022222
 */
public abstract class MessageBizService {

    /**
     * 定义流程模板，final修饰防止覆写
     */
    public final void doSend(String msg) {

        //===1.打印日志
        printLog();
        //===2.发消息
        send(msg);
        //===3.记录日志及缓存发送频率
        afterSendSuccess();
    }


    //通用方法，不能被扩展
    private void printLog() {
        System.out.println("开始发送消息");
    }

    //抽象方法，子类必须覆盖扩展
    protected abstract void send(String msg);

    //钩子方法，子类可选覆盖扩展
    public void afterSendSuccess() {
    }


    public static void main(String[] args) {
        MessageTypeEnum.SMS.getService().doSend("hello world");
    }
}
