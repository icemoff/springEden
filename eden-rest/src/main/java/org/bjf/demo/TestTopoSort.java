package org.bjf.demo;

import java.util.ArrayList;
import java.util.List;

/**
 * 拓扑排序
 */
public class TestTopoSort {
    public static void main(String[] args) {
        List<String> lines = new ArrayList<>();
        lines.add("X,Y");
        lines.add("B,C");
        lines.add("A,B");
//        lines.add("C,A");
        lines.add("E,F");

        DirectedGraph directedGraph = new DirectedGraph(lines);
        try {
            directedGraph.topoSort();
        } catch (Exception e) {
            System.out.println("graph has circle");
            e.printStackTrace();
        }

    }
}