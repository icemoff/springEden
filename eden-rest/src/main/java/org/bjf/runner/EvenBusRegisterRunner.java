package org.bjf.runner;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * event bus 事件注册
 *
 * @author bjf on 2022/2/28.
 * @version 1.022222
 */
@Slf4j
@Component
public class EvenBusRegisterRunner implements ApplicationRunner {
    @Override
    public void run(ApplicationArguments args) {
        log.info(">>>>>>>>>>>>>>>服务启动执行，执行event bus事件注册");
//        EventBusUtil.register(ProcessResultListener.getInstance());
    }
}
