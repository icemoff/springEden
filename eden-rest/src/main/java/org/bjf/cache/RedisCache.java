package org.bjf.cache;

import com.alibaba.fastjson2.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateUtils;
import org.bjf.exception.CommMsgCode;
import org.bjf.exception.ServiceException;
import org.bjf.utils.SpringUtil;
import org.springframework.cache.Cache;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author bjf
 */
@Slf4j
public class RedisCache implements Cache {

    protected RedisTemplate redis;
    private Map<Object, ReentrantLock> locks = new ConcurrentHashMap<>();
    /**
     * 缓存名称
     */
    private String name;

    /**
     * 超时时间（秒）,默认一天
     */
    private long timeout = 3600 * 24L;

    public RedisCache(RedisTemplate redis, String name, long timeout) {
        this.redis = redis;
        this.name = name;
        this.timeout = timeout;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public Object getNativeCache() {
        return this.redis;
    }

    @Override
    public ValueWrapper get(Object key) {
        final Object value = readFromRedis(key, null);
        if (value != null) {
            return () -> value;
        }
        return null;
    }

    @Override
    public <T> T get(Object key, Class<T> clz) {
        Object value = readFromRedis(key, null);
        if (value != null) {
            return JSON.parseObject(value.toString(), clz);
        }
        return null;
    }

    /**
     * 只有在使用 @Cacheable 时设置 sync 值为 true 时才会调用到此方法,且些方法有异常不会转到默认处理
     */
    @Override
    public <T> T get(Object key, Callable<T> callable) {
        try {
            Object value = readFromRedis(key, callable);
            if (value != null) {
                return (T) value;
            }
            value = callable.call();
            put(key, value);
            return (T) value;
        } catch (Exception e) {
            log.info("redis异常", e);
            throw new ServiceException(CommMsgCode.DB_ERROR, "redis异常");
        }
    }

    @Override
    public void put(Object key, Object value) {
        writeToRedis(key, value);
    }

    @Override
    public ValueWrapper putIfAbsent(Object key, Object value) {
        ValueWrapper cacheValue = this.get(key);
        if (cacheValue == null) {
            this.put(key, value);
            return null;
        } else {
            return () -> cacheValue;
        }
    }

    @Override
    public void evict(Object key) {
        redis.delete(key);
    }

    @Override
    public void clear() {

    }

    private Object readFromRedis(Object key, Callable callable) {
        Object value = redis.opsForValue().get(key);
        if (value == null) {
            return null;
        }
        CacheModel cm = (CacheModel) value;
        Long expireTime = cm.getExpireTime();
        // 如果还有3秒即将过期，单线程去刷新数据库
        if (callable != null && DateUtils.addSeconds(new Date(), 3).getTime() > expireTime) {
            String lockKey = "live:cache:lock:" + key;
            Boolean lock = redis.opsForValue().setIfAbsent(lockKey, "lock", 5, TimeUnit.SECONDS);
            if (lock) {
                TaskExecutor executor = SpringUtil.getBean("asyncTaskExecutor", TaskExecutor.class);
                executor.execute(() -> {
                    try {
                        log.info("缓存即将过期，提前更新缓存");
                        Object call = callable.call();
                        put(key, call);
                    } catch (Exception e) {
                        log.error("预加载缓存失败，name=" + getName() + ",key=" + key, e);
                    } finally {
                        redis.delete(lockKey);
                    }
                });
            }
        }
        return cm.getData();
    }

    private void writeToRedis(Object key, Object value) {
        redis.opsForValue()
                .set(key, new CacheModel(value, System.currentTimeMillis() + timeout * 1000L), timeout,
                        TimeUnit.SECONDS);
    }
}
