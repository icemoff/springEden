package org.bjf.cache;


import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 缓存模型
 */
@Data
@Accessors(chain = true)
public class CacheModel<T> {

    private T data;
    private Long expireTime;

    public CacheModel() {
    }

    public CacheModel(T data, Long expireTime) {
        this.data = data;
        this.expireTime = expireTime;
    }
}
