package org.bjf.cache;

/**
 * 初衷是所有配置使用spring cache 框架（@CacheAble）的缓存名称要明确定义（如：过期时间），然后再使用
 */
public class CacheNameConfig {

    /**
     * ===========================redis start ===========================
     */
    /**
     * 明确声明使用cache name 的过期时间
     */
    public enum RedisCacheEnum {

        FAST(5),
        USER(10),
        SYS(86400),
        MENU(3600);

        /**
         * 过期时间秒
         */
        private long timeout = 10L;

        RedisCacheEnum(long timeout) {
            this.timeout = timeout;
        }

        public long getTimeout() {
            return timeout;
        }

        /**
         * 缓存value,对应@Cacheable里面的value值
         * <p>
         * 常量值等于RedisCacheEnum的枚举name值 注：此类和RedisCacheEnum有冗余，各位有什么巧妙的办法？
         */
        public interface Names {

            String FAST = "FAST";
            String USER = "USER";
            String SYS = "SYS";
            String MENU = "MENU";
        }
    }

    /**
     * ===========================redis end ===========================
     */

    /**
     * ===========================local start ===========================
     */
    /**
     * 明确声明使用cache name 的过期时间和容量
     */
    public enum LocalCacheEnum {
        //有效期5秒,最大容量1000
        FAST_LOCAL(5, 1000),
        //缺省10分钟,最大容量30000
        LOCAL(600, 30000),
        ;

        private long timeout = 10;
        private int capacity = 1000;

        LocalCacheEnum(int timeout, int capacity) {
            this.timeout = timeout;
            this.capacity = capacity;
        }

        public long getTimeout() {
            return timeout;
        }

        public int getCapacity() {
            return capacity;
        }

        /**
         * 缓存value,对应@Cacheable里面的value值
         * <p>
         * 常量值等于LocalCacheEnum的枚举name值 注：此类和LocalCacheEnum有冗余，各位有什么巧妙的办法？
         */
        public interface Names {

            public static final String FAST_LOCAL = "FAST_LOCAL";
            public static final String LOCAL = "LOCAL";
        }
    }
    /**
     * ===========================local end ===========================
     */

    /**
     * ===========================二级缓存配置（caffeine + redis） start ===========================
     */

    /**
     * 明确声明使用cache name 的过期时间和容量
     */
    public enum SecondaryCacheEnum {
        //有效期5秒,最大容量1000
        VEDEO(5, 1000),
        //缺省10分钟,最大容量30000
        COMPUTOR(30, 30000),
        ;

        private long timeout = 10;
        private int capacity = 1000;

        SecondaryCacheEnum(int timeout, int capacity) {
            this.timeout = timeout;
            this.capacity = capacity;
        }

        public long getTimeout() {
            return timeout;
        }

        public int getCapacity() {
            return capacity;
        }


        /**
         * 缓存value,对应@Cacheable里面的value值
         * <p>
         * 常量值等于SecondaryCacheEnum的枚举name值 注：此类和SecondaryCacheEnum有冗余，各位有什么巧妙的办法？
         */
        public interface Names {

            public static final String VEDEO = "VEDEO";
            public static final String COMPUTOR = "COMPUTOR";
        }
    }
    /**
     * ===========================二级缓存配置（caffeine + redis） end ===========================
     */
}