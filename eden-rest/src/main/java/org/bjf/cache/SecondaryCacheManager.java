package org.bjf.cache;

import com.github.benmanes.caffeine.cache.Caffeine;
import org.bjf.cache.CacheNameConfig.SecondaryCacheEnum;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

/**
 * @author bjf
 * @date 2018/1/8
 */
public class SecondaryCacheManager implements CacheManager {

    private final ConcurrentMap<String, Cache> cacheMap = new ConcurrentHashMap<>(16);
    private final RedisTemplate redisTemplate;

    public SecondaryCacheManager(RedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
        initCacheMap();
    }

    private void initCacheMap() {
        for (SecondaryCacheEnum cache : SecondaryCacheEnum.values()) {
            cacheMap.put(cache.name(), this.createCache(cache));
        }
    }

    private Cache createCache(SecondaryCacheEnum cache) {

        //===1.创建本地缓存，比redis缓存多2秒
        CaffeineCache caffeineCache = new CaffeineCache(cache.name(),
                Caffeine.newBuilder().recordStats()
                        .expireAfterWrite(cache.getTimeout() + 2, TimeUnit.SECONDS)
                        .maximumSize(cache.getCapacity())
                        .build());

        //===2.创建redis缓存
        RedisCache redisCache = new RedisCache(redisTemplate, cache.name(), cache.getTimeout());

        //===3.返回二级缓存对象
        return new SecondaryCache(cache.name(), caffeineCache, redisCache, Boolean.FALSE);
    }


    @Override
    public Cache getCache(String name) {
        Cache cache = this.cacheMap.get(name);
        return cache;
    }

    @Override
    public Collection<String> getCacheNames() {
        return Collections.unmodifiableSet(this.cacheMap.keySet());
    }
}
