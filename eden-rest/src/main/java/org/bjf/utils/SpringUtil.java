package org.bjf.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * spring工具类，用于非容器管理Bean取springContext(业务层面尽量不要使用)
 *
 * @author bjf
 */
@Component
public class SpringUtil implements ApplicationContextAware {

    private static ApplicationContext ac;

    public static ApplicationContext getApplicationContext() {
        return ac;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ac = applicationContext;
    }

    public static <T> T getBean(Class<T> clz) {
        return ac.getBean(clz);
    }

    public static <T> T getBean(String name, Class<T> clz) {
        return ac.getBean(name, clz);
    }
}
