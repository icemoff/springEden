package org.bjf.utils;

import org.bjf.exception.MsgCode;
import org.bjf.exception.ServiceException;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;

public class ExceptionAssert {

    public static void isTrue(boolean expression, String message) {
        if (!expression) {
            throw new ServiceException(message);
        }
    }

    public static void isTrue(boolean expression, MsgCode msgCode) {
        if (!expression) {
            throw new ServiceException(msgCode);
        }
    }

    public static void isTrue(boolean expression, MsgCode msgCode, String message) {
        if (!expression) {
            throw new ServiceException(msgCode, message);
        }
    }

    public static void isFalse(boolean expression, String message) {
        if (expression) {
            throw new ServiceException(message);
        }
    }

    public static void isFalse(boolean expression, MsgCode msgCode) {
        if (expression) {
            throw new ServiceException(msgCode);
        }
    }

    public static void isFalse(boolean expression, MsgCode msgCode, String message) {
        if (expression) {
            throw new ServiceException(msgCode, message);
        }
    }

    public static void notNull(Object obj, String message) {
        isTrue(!Objects.isNull(obj), message);
    }

    public static void notNull(Object obj, MsgCode msgCode) {
        isTrue(!Objects.isNull(obj), msgCode);
    }

    public static void notNull(Object obj, MsgCode msgCode, String message) {
        isTrue(!Objects.isNull(obj), msgCode, message);
    }

    public static void notEmpty(Collection collection, String message) {
        isTrue(!CollectionUtils.isEmpty(collection), message);
    }

    public static void notEmpty(Collection collection, MsgCode msgCode) {
        isTrue(!CollectionUtils.isEmpty(collection), msgCode);
    }

    public static void notEmpty(Map map, String message) {
        isTrue(!CollectionUtils.isEmpty(map), message);
    }

    public static void notEmpty(Map map, MsgCode msgCode) {
        isTrue(!CollectionUtils.isEmpty(map), msgCode);
    }


    public static void notEmpty(Object[] array, String message) {
        isTrue(!ObjectUtils.isEmpty(array), message);
    }

    public static void notEmpty(Object[] array, MsgCode msgCode) {
        isTrue(!ObjectUtils.isEmpty(array), msgCode);
    }
}
