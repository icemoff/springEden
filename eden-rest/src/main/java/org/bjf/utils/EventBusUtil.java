package org.bjf.utils;

import com.google.common.eventbus.EventBus;

/**
 * 进程级别的消息通知机制
 * @author bjf
 */
public class EventBusUtil {

    private static EventBus eventBus;

    static {
        eventBus = new EventBus("even-bus");
    }

    public static void register(Object obj) {
        eventBus.register(obj);
    }

    public static void post(Object obj) {
        eventBus.post(obj);
    }

    public static void asyncPost(Object obj) {
        eventBus.post(obj);
    }

}
