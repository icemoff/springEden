package org.bjf.utils;

/**
 * @author jicexosl
 * @date 2018/3/9
 */
public interface StorageConfig {
    //String METRIC_INDEX_NAME = "<amp_metric-{now/M{YYYY.MM}}";
    String METRIC_INDEX_NAME = "test_amp_metric";
    String METRIC_TYPE_NAME = "metric";

    //String METRIC_SEARCH_INDEX_NAME = "amp_metric-*";

    String METRIC_SEARCH_INDEX_NAME = "test_amp_metric";

    //String BASELINE_INDEX_NAME = "amp_baseline";

    String BASELINE_INDEX_NAME = "test_amp_baseline";
    String BASELINE_TYPE_NAME = "baseline";
}
