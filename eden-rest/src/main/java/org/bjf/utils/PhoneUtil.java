package org.bjf.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * @author binjinfeng
 */
public class PhoneUtil {

    /**
     * 手机号脱敏筛选正则
     */
    public static final String PHONE_BLUR_REGEX = "(\\w{3})\\d{4}(\\w{4})";

    /**
     * 手机号脱敏替换正则
     */
    public static final String PHONE_BLUR_REPLACE_REGEX = "$1****$2";

    /**
     * 手机号脱敏处理
     */
    public static final String blurPhone(String phone) {

        if (StringUtils.isBlank(phone)) {
            return "";
        }
        return phone.replaceAll(PHONE_BLUR_REGEX, PHONE_BLUR_REPLACE_REGEX);
    }

}
