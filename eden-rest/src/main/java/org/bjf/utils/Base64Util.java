package org.bjf.utils;

import org.apache.commons.codec.binary.Base64;

/**
 * Created by lenovo on 2017/8/24.
 */
public class Base64Util {

    /**
     * 解码
     */
    public static String decode(String text) {
        return new String(Base64.decodeBase64(text));
    }

    /**
     * 编码
     */
    public static String encode(String text) {
        return new String(Base64.encodeBase64(text.getBytes()));
    }

    /**
     * 编码-url safe
     */
    public static String encodeURLSafe(String text) {
        return Base64.encodeBase64URLSafeString(text.getBytes());
    }

}