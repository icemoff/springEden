package org.bjf.utils;

import org.slf4j.MDC;

public class TraceUtil {

    private static final String TRACE_ID = "X-B3-TraceId";

    public static String getTraceId() {
        return MDC.get(TRACE_ID);
    }
}
