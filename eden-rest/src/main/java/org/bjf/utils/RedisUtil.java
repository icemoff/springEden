package org.bjf.utils;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import lombok.extern.slf4j.Slf4j;
import org.bjf.config.ScriptConfig;
import org.redisson.api.RLock;
import org.redisson.api.RReadWriteLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

/**
 * @author bjf
 **/
@Component
@Slf4j
@SuppressWarnings(value = { "unchecked", "rawtypes" })
public class RedisUtil {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    protected RedisTemplate redisTemplate;

    @Autowired
    private RedissonClient redissonClient;

    /**
     * 指定缓存失效时间
     *
     * @param key     键
     * @param timeout 时间(秒)
     */
    public void expire(String key, long timeout) {
        if (timeout > 0) {
            stringRedisTemplate.expire(key, Duration.ofSeconds(timeout));
        }
    }

    /**
     * 根据key 获取过期时间(秒)
     */
    public Long getExpire(String key) {
        return stringRedisTemplate.getExpire(key, TimeUnit.SECONDS);
    }

    /**
     * 判断key是否存在
     */
    public Boolean hasKey(String key) {
        return stringRedisTemplate.hasKey(key);
    }

    /**
     * 删除缓存
     */
    public void del(String... key) {
        if (key != null && key.length > 0) {
            stringRedisTemplate.delete(Arrays.asList(key));
        }
    }

    //============================String=============================

    /**
     * 普通缓存获取
     */
    public String get(String key) {
        return key == null ? null : stringRedisTemplate.opsForValue().get(key);
    }

    /**
     * 普通缓存放入( 都应该设置过期时间，正常不应该使用此方法)
     */
    public void set(String key, String value) {
        stringRedisTemplate.opsForValue().set(key, value);
    }

    /**
     * 普通缓存放入并设置时间
     *
     * @param key     键
     * @param value   值
     * @param timeout 时间(秒) time要大于0 如果time小于等于0 将设置无限期
     */
    public void set(String key, String value, long timeout) {
        if (timeout > 0) {
            stringRedisTemplate.opsForValue().set(key, value, Duration.ofSeconds(timeout));
        } else {
            set(key, value);
        }
    }

    public void setObj(String key, Object value, long timeout) {
        redisTemplate.opsForValue().set(key, value, Duration.ofSeconds(timeout));
    }

    public <T> T getObj(String key) {
        return (T) redisTemplate.opsForValue().get(key);
    }

    /**
     * 离线缓存
     */
    Cache<String, Object> offlineCache = Caffeine.newBuilder()
            .expireAfterWrite(60, TimeUnit.SECONDS)
            .maximumSize(10)
            .build();
    public <T> T getObj(String key,  Supplier loader) {
        T value;
        try {
            value = (T) redisTemplate.opsForValue().get(key);
        } catch (Exception e) {
            log.error("redis服务器异常");
            Object obj = offlineCache.get(key, t -> loader.get());
            try {
                setObj(key, obj, 600);
            } catch (Exception ignore) {
            }
            return (T) obj;
        }
        return value;
    }

    /**
     * 递增
     */
    public Long incr(String key, long num) {
        return stringRedisTemplate.opsForValue().increment(key, num);
    }

    public Double incr(String key, double num) {
        return stringRedisTemplate.opsForValue().increment(key, num);
    }

    //================================Hash=================================

    /**
     * HashGet
     */
    public Object hget(String key, String item) {
        return stringRedisTemplate.opsForHash().get(key, item);
    }

    /**
     * 获取hashKey对应的所有键值
     */
    public Map<Object, Object> hmget(String key) {
        return stringRedisTemplate.opsForHash().entries(key);
    }

    /**
     * HashSet(都要设置过期时间，正常不应该使用此接口)
     */
    public void hmset(String key, Map<String, String> map) {
        stringRedisTemplate.opsForHash().putAll(key, map);
    }

    /**
     * HashSet 并设置时间
     *
     * @param key  键
     * @param map  对应多个键值
     * @param time 时间(秒)
     */
    public void hmset(String key, Map<String, String> map, long time) {
        stringRedisTemplate.opsForHash().putAll(key, map);
        if (time > 0) {
            expire(key, time);
        }
    }

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key   键
     * @param item  项
     * @param value 值
     */
    public void hset(String key, String item, String value) {
        stringRedisTemplate.opsForHash().put(key, item, value);
    }

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @param time  时间(秒)  注意:如果已存在的hash表有时间,这里将会替换原有的时间
     */
    public void hset(String key, String item, String value, long time) {
        stringRedisTemplate.opsForHash().put(key, item, value);
        if (time > 0) {
            expire(key, time);
        }
    }

    /**
     * 删除hash表中的值
     *
     * @param key  键 不能为null
     * @param item 项 可以使多个 不能为null
     */
    public void hdel(String key, Object... item) {
        stringRedisTemplate.opsForHash().delete(key, item);
    }

    /**
     * 判断hash表中是否有该项的值
     *
     * @param key  键 不能为null
     * @param item 项 不能为null
     */
    public boolean hHasKey(String key, String item) {
        return stringRedisTemplate.opsForHash().hasKey(key, item);
    }

    /**
     * hash递增 如果不存在,就会创建一个 并把新增后的值返回
     */
    public double hincr(String key, String item, double number) {
        return stringRedisTemplate.opsForHash().increment(key, item, number);
    }


    //============================set=============================

    /**
     * 根据key获取Set中的所有值
     */
    public Set<String> sGet(String key) {
        return stringRedisTemplate.opsForSet().members(key);
    }

    /**
     * 根据value从一个set中查询,是否存在
     */
    public Boolean sHasKey(String key, String value) {
        return stringRedisTemplate.opsForSet().isMember(key, value);
    }

    /**
     * 将数据放入set缓存
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 成功个数
     */
    public Long sSet(String key, String... values) {
        try {
            return stringRedisTemplate.opsForSet().add(key, values);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return 0L;
        }
    }

    /**
     * 将set数据放入缓存
     *
     * @param key    键
     * @param time   时间(秒)
     * @param values 值 可以是多个
     * @return 成功个数
     */
    public Long sSetAndTime(String key, long time, String... values) {
        try {
            Long count = stringRedisTemplate.opsForSet().add(key, values);
            if (time > 0) {
                expire(key, time);
            }
            return count;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return 0L;
        }
    }

    /**
     * 获取set缓存的长度
     */
    public Long sGetSetSize(String key) {
        return stringRedisTemplate.opsForSet().size(key);
    }

    /**
     * 移除值为value的
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 移除的个数
     */
    public Long setRemove(String key, String... values) {
        try {
            return stringRedisTemplate.opsForSet().remove(key, values);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return 0L;
        }
    }
    //===============================list=================================

    /**
     * 获取list缓存的内容
     *
     * @param key   键
     * @param start 开始
     * @param end   结束  0 到 -1代表所有值
     */
    public List<String> lGet(String key, long start, long end) {
        return stringRedisTemplate.opsForList().range(key, start, end);
    }

    /**
     * 获取list缓存的长度
     *
     * @param key 键
     */
    public Long lGetListSize(String key) {
        return stringRedisTemplate.opsForList().size(key);
    }

    /**
     * 通过索引 获取list中的值
     *
     * @param key   键
     * @param index 索引  index>=0时， 0 表头，1 第二个元素，依次类推；index<0时，-1，表尾，-2倒数第二个元素，依次类推
     */
    public String lGetIndex(String key, long index) {
        return stringRedisTemplate.opsForList().index(key, index);
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     */
    public void lSet(String key, String value) {
        stringRedisTemplate.opsForList().rightPush(key, value);
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒)
     */
    public void lSet(String key, String value, long time) {
        stringRedisTemplate.opsForList().rightPush(key, value);
        if (time > 0) {
            expire(key, time);
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     */
    public void lSet(String key, List<String> value) {
        stringRedisTemplate.opsForList().rightPushAll(key, value);
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒)
     */
    public void lSet(String key, List<String> value, long time) {
        stringRedisTemplate.opsForList().rightPushAll(key, value);
        if (time > 0) {
            expire(key, time);
        }
    }

    /**
     * 根据索引修改list中的某条数据
     *
     * @param key   键
     * @param index 索引
     * @param value 值
     */
    public void lUpdateIndex(String key, long index, String value) {
        stringRedisTemplate.opsForList().set(key, index, value);
    }

    /**
     * 移除N个值为value
     *
     * @param key   键
     * @param count 移除多少个
     * @param value 值
     * @return 移除的个数
     */
    public Long lRemove(String key, long count, String value) {
        try {
            return stringRedisTemplate.opsForList().remove(key, count, value);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return 0L;
        }
    }

    /**
     * 模糊查询获取key值
     */
    public Set keys(String pattern) {
        return stringRedisTemplate.keys(pattern);
    }

    /**
     * 使用Redis的消息队列
     *
     * @param channel
     * @param message 消息内容
     */
    public void convertAndSend(String channel, String message) {
        stringRedisTemplate.convertAndSend(channel, message);
    }


    /**
     * 根据起始结束序号遍历Redis中的list
     *
     * @param listKey
     * @param start   起始序号
     * @param end     结束序号
     */
    public List<String> rangeList(String listKey, long start, long end) {
        //绑定操作
        BoundListOperations<String, String> boundValueOperations = stringRedisTemplate.boundListOps(listKey);
        //查询数据
        return boundValueOperations.range(start, end);
    }

    /**
     * 弹出右边的值 --- 并且移除这个值
     *
     * @param listKey
     */
    public String rifhtPop(String listKey) {
        //绑定操作
        BoundListOperations<String, String> boundValueOperations = stringRedisTemplate.boundListOps(listKey);
        return boundValueOperations.rightPop();
    }


    //===============================lock=================================

    /**
     * @param timeout 过期时间（秒）
     */
    public boolean tryLock(String key, long timeout) {
        RLock lock = redissonClient.getLock(key);
        try {
            return lock.tryLock(timeout, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
        }
        return false;
    }

    public void unlock(String key) {
        RLock lock = redissonClient.getLock(key);
        lock.unlock();
    }

    public RReadWriteLock rwLock(String key) {
        return redissonClient.getReadWriteLock(key);
    }

    /**
     * ===========================redis zet 排行榜===========================
     */
    public void zAdd(String key, String item, double score) {
        stringRedisTemplate.opsForZSet().add(key, item, score);
    }

    /**
     * 查询排名
     */
    public Set zReverseRange(String key, long start, long end) {
        return stringRedisTemplate.opsForZSet().reverseRange(key, start, end);
    }

    /**
     * 查询排名列表
     */
    public Set<ZSetOperations.TypedTuple<String>> zReverseRangeWithScores(String key, long start, long end) {
        return stringRedisTemplate.opsForZSet().reverseRangeWithScores(key, start, end);
    }

    /**
     * 查询分数
     */
    public Double zScore(String key, Object item) {
        return stringRedisTemplate.opsForZSet().score(key, item);
    }

    /**
     * 查询个人排名
     */
    public Long zReverseRank(String key, Object item) {
        return stringRedisTemplate.opsForZSet().reverseRank(key, item);
    }

    /**
     * ===========================lua 脚本===========================
     */
    /**
     * 基于redis lua脚本的分布式锁
     */
    public boolean getLockLua(String key, long timeout) {
        List<String> keys = Arrays.asList(key);
        try {
            Long lockVal = (Long) stringRedisTemplate.execute(ScriptConfig.LOCK_SCRIPT, keys, timeout);
            if (lockVal > 0) {
                return Boolean.TRUE;
            }
        } catch (Exception e) {
            log.error("获取锁异常", e);
        }
        return Boolean.FALSE;
    }
}