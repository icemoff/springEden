package org.bjf.utils;

import lombok.Data;

@Data
public class HttpObj {

    private Integer statusCode;

    private String body;

    public HttpObj() {

    }

    public HttpObj(Integer statusCode, String body) {
        this.statusCode = statusCode;
        this.body = body;
    }


}
