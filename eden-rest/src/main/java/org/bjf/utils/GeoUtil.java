package org.bjf.utils;

import com.spatial4j.core.io.GeohashUtils;

/**
 * Created by lenovo on 2017/10/29.
 */
public class GeoUtil {

    /**
     * 根据经纬度获取geoHash值
     * geo_code LIKE CONCAT(?, '%');
     *
     * @param lng 经度
     * @param lat 纬度
     * @return geoHash
     */
    public static String getGeoCode(Double lng, Double lat) {
        if (lng != null && lat != null && lng > 0 && lat > 0) {
            return GeohashUtils.encodeLatLon(lat, lng);
        }
        return "";
    }
}
