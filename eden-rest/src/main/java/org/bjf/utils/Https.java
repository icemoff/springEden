package org.bjf.utils;

import com.alibaba.fastjson2.JSON;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.AuthSchemes;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.config.SocketConfig;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;

public class Https {

    public static final String DEFAULT_CHARSET = "UTF-8";
    private static Logger log = LoggerFactory.getLogger(Https.class);
    private static CloseableHttpClient httpClient;
    private static PoolingHttpClientConnectionManager cm;
    private static RequestConfig requestConfig;
    private static ResponseHandler<HttpObj> responseHandler = null;

    static {

        //===1.Registry配置
        Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder
                .<ConnectionSocketFactory>create()
                .register("http", PlainConnectionSocketFactory.INSTANCE)
                .register("https", createSSLConnectionSocketFactory())
                .build();

        //===2.请求配置
        requestConfig = RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD_STRICT)
                .setExpectContinueEnabled(true)
                .setTargetPreferredAuthSchemes(Arrays.asList(AuthSchemes.NTLM, AuthSchemes.DIGEST))
                .setProxyPreferredAuthSchemes(Arrays.asList(AuthSchemes.BASIC))
                .build();

        //===3.设置ConnectionManager
        cm = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
        // 最大连接数
        cm.setMaxTotal(50);
        // 每路由的最大连接数，默认值是2
        cm.setDefaultMaxPerRoute(10);
        //剔除过期无效连接
        cm.closeExpiredConnections();
        cm.setValidateAfterInactivity(2000);

        //===4.设置httpClient
        httpClient = HttpClientBuilder.create()
                .useSystemProperties()
                // 线程池
                .setConnectionManager(cm)
                // 请求设置
                .setDefaultRequestConfig(requestConfig)
                // 超时
                .setDefaultSocketConfig(SocketConfig.custom().setSoTimeout(5000).build())
                .build();

        //===5.响应设置
        responseHandler = new ResponseHandler<HttpObj>() {
            @Override
            public HttpObj handleResponse(HttpResponse httpResponse) throws IOException {
                StatusLine statusLine = httpResponse.getStatusLine();
                HttpEntity entity = httpResponse.getEntity();
                final byte[] bodyBytes;
                if (entity != null) {
                    bodyBytes = EntityUtils.toByteArray(entity);
                } else {
                    bodyBytes = new byte[0];
                }
                HttpObj resp = new HttpObj();
                resp.setStatusCode(statusLine.getStatusCode());
                resp.setBody(new String(bodyBytes, DEFAULT_CHARSET));
                return resp;
            }
        };

        //===5.关闭钩子
        shutdownHook();
    }

    private static SSLConnectionSocketFactory createSSLConnectionSocketFactory() {
        //===1.重写验证方法，取消检测SSL
        TrustManager manager = new X509TrustManager() {
            @Override
            public void checkClientTrusted(X509Certificate[] x509Certificates, String s)
                    throws CertificateException {

            }

            @Override
            public void checkServerTrusted(X509Certificate[] x509Certificates, String s)
                    throws CertificateException {

            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }
        };

        SSLConnectionSocketFactory factory = null;
        try {
            SSLContext sslContext = SSLContext.getInstance("TLS");

            sslContext.init(null, new TrustManager[]{manager}, null);
            factory = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return factory;
    }


    private static void shutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread("thread-http.shutdown") {
            @Override
            public void run() {
                try {
                    httpClient.close();
                } catch (IOException ignore) {
                    log.error("httpClient 关闭失败", ignore);
                }
            }
        });
    }

    public static HttpObj get(String url) {
        return get(url, null);
    }

    public static HttpObj get(String url, Map<String, Object> params) {
        return getHttpObj(url, params, null);
    }

    public static HttpObj get(String url, Map<String, Object> params, Map<String, String> headers) {
        return getHttpObj(url, params, headers);
    }

    public static HttpObj getHttpObj(String url, Map<String, Object> params,
                                     Map<String, String> headers) {

        //===1.组装url
        if (params != null && params.size() > 0) {
            List<NameValuePair> nvps = new ArrayList<>(params.size());
            for (Map.Entry<String, Object> param : params.entrySet()) {
                nvps.add(new BasicNameValuePair(param.getKey(), param.getValue().toString()));
            }
            String queryString = URLEncodedUtils.format(nvps, DEFAULT_CHARSET);
            url = url + "?" + queryString;
        }

        HttpGet get = new HttpGet(url);
        get.setConfig(requestConfig);

        return execute(get, headers);
    }


    public static HttpObj post(String url, Map<String, Object> params) {
        return post(url, params, null);
    }

    public static HttpObj post(String url, Map<String, Object> params, Map<String, String> headers) {
        UrlEncodedFormEntity entity = null;
        if (params != null && params.size() > 0) {
            List<NameValuePair> nvps = new ArrayList<>(params.size());
            for (Map.Entry<String, Object> param : params.entrySet()) {
                nvps.add(new BasicNameValuePair(param.getKey(), param.getValue().toString()));
            }
            entity = new UrlEncodedFormEntity(nvps, Charset.forName(DEFAULT_CHARSET));
        }
        return post(url, entity, headers);
    }

    public static HttpObj postJson(String url, String body) {
        return postJson(url, body, null);
    }

    public static HttpObj postJson(String url, String body, Map<String, String> headers) {
        StringEntity entity = new StringEntity(body, DEFAULT_CHARSET);
        entity.setContentType("application/json");
        return post(url, entity, headers);
    }

    private static HttpObj post(String url, HttpEntity entity, Map<String, String> headers) {
        HttpPost post = new HttpPost(url);
        post.setConfig(requestConfig);
        if (entity != null) {
            post.setEntity(entity);
        }
        return execute(post, headers);
    }

    private static HttpObj execute(HttpUriRequest req, Map<String, String> headers) {
        //===请求header
        if (headers != null && headers.size() > 0) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                req.setHeader(entry.getKey(), entry.getValue());
            }
        }
        //===traceId
        String traceId = MDC.get("X-B3-TraceId");
        if (StringUtils.isNotBlank(traceId)) {
            req.setHeader("X-B3-TraceId", traceId);
        }
        try {
            return httpClient.execute(req, responseHandler);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {

        //===1.get请求
        String ua = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.146 Safari/537.36";
        String get = Https.get("https://www.oschina.net/", null, ImmutableMap.of("User-Agent", ua)).getBody();
        System.out.println("get = " + get);

        //===2.post for json
        Map<String, Object> param = new HashMap<>();
        param.put("name", "google");
        String json = JSON.toJSONString(param);
        System.out.println("json = " + json);
        String post = Https.postJson("http://localhost:9095/test/add", json).getBody();
        System.out.println("post = " + post);

        //===3.post common
        String body = Https.post("http://localhost:9095/test/add", param).getBody();
        System.out.println("body = " + body);
    }

}
