package org.bjf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("classpath:spring-job.xml")
//@EnableLogFilter
public class Application {

    public static ConfigurableApplicationContext sContext;

    public static void main(String[] args) {
        sContext = SpringApplication.run(Application.class, args);
    }
}
