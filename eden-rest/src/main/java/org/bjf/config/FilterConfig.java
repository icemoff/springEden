package org.bjf.config;

import org.bjf.demo.enableclz.LogFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author bjf
 */
@Configuration
public class FilterConfig {
    @Bean
    public FilterRegistrationBean<LogFilter> logFilter() {
        return new FilterRegistrationBean<>(new LogFilter());
    }
}
