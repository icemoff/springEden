package org.bjf.config;

import org.bjf.aop.WebsocketInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

/**
 * @author bjf
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    @Autowired
    private WebsocketInterceptor websocketInterceptor;

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        //注册一个stomp的节点
        registry.addEndpoint("/websocketServer")
                //允许跨域
                .setAllowedOrigins("*");
        //拦截器
//        .addInterceptors(websocketInterceptor)
        // 允许使用SockJS访问
//        .withSockJS();
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        // 订阅Broker名称 有这些前缀的会路由到broker
        registry.enableSimpleBroker("/topic", "/queue");
        // 全局使用的消息前缀（客户端订阅路径上会体现再来）,有这些前缀的会被到有@SubscribeMapping与@MessageMapping的业务方法拦截
        // 首先会被路由到@SubscribeMapping与@MessageMapping的业务方法中, 进行处理, 之后再到broker.
//    registry.setApplicationDestinationPrefixes("/app");
        // 点对点使用订阅前缀（客户端订阅路径上会体现再来）,不设置的话，默认也是/user/
//    registry.setUserDestinationPrefix("/user/");
    }
}
