package org.bjf.config;

import cn.dev33.satoken.interceptor.SaInterceptor;
import cn.dev33.satoken.router.SaRouter;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.bjf.aop.*;
import org.bjf.security.satoken.SaTokenAdminUtil;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;
import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

/**
 * @author bjf
 */
@Configuration
@ConditionalOnClass(WebMvcConfigurer.class)
@Order(Ordered.HIGHEST_PRECEDENCE)
public class WebConfig implements WebMvcConfigurer {

    @Resource
    private LogInterceptor logInterceptor;
    @Resource
    private ApiInterceptor apiInterceptor;
    @Resource
    private AdminInterceptor adminInterceptor;
    @Resource
    private SignInterceptor signInterceptor;

    /**
     * 拦截器
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
//    registry.addInterceptor(spanCustomizingAsyncHandlerInterceptor).addPathPatterns("/**");
//        registry.addInterceptor(logInterceptor).addPathPatterns("/**");
//        registry.addInterceptor(apiInterceptor).addPathPatterns("/api/**");
//        registry.addInterceptor(adminInterceptor).addPathPatterns("/admin/**")
//                .excludePathPatterns("/admin/login");
//    registry.addInterceptor(signInterceptor).addPathPatterns("/**");

        //====PC admin 管理台的拦截器
        registry.addInterceptor(new SaInterceptor(afterHandler -> {
                    // 登录检查
                    SaRouter.match("/**")    // 拦截的 path 列表，可以写多个 */
                            .check(r -> SaTokenAdminUtil.checkLogin());

                    // 如果没过期，续签10分钟
                    long tokenTimeout = SaTokenAdminUtil.getTokenTimeout();
                    if (tokenTimeout > 0 && tokenTimeout<600) {
                        SaTokenAdminUtil.renewTimeout(600);// 用于token续期
                    }

                    // 超级管理员才能访问此模块
                    //SaRouter.match("/admin/order/**", r -> SaTokenAdminUtil.checkRole("super-admin"));
                }))
                .addPathPatterns("/admin/**")
                .excludePathPatterns("/admin/login");
    }

    @Bean
    public MultiPostMethodResolver multiPostMethodResolver() {
        return new MultiPostMethodResolver();
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(multiPostMethodResolver());
    }

    /**
     * cors 跨域支持 可以用@CrossOrigin在controller上单独设置
     */
    @Bean
    public FilterRegistrationBean<CorsFilter> corsFilter() {
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        // 设置你要允许的网站域名，如果全允许则设为 *
        config.addAllowedOriginPattern("*");
        // 如果要限制 HEADER 或 METHOD 请自行更改
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config);
        FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<>(new CorsFilter(source));
        // 这个顺序很重要，为避免麻烦请设置在最前
        bean.setOrder(0);

        return bean;
    }


    @Bean
    public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter(ObjectMapper objectMapper) {
        MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
        /**
         * 序列换成json时,将所有的long变成string
         * 因为js中得数字类型不能包含所有的java long值
         */
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addSerializer(BigDecimal.class, ToStringSerializer.instance);
        simpleModule.addSerializer(BigInteger.class, ToStringSerializer.instance);
        simpleModule.addSerializer(Long.class, ToStringSerializer.instance);
        simpleModule.addSerializer(Long.TYPE, ToStringSerializer.instance);
        // @RequestBody 日期类型接收处理
        simpleModule.addDeserializer(Date.class, RequestBodyDateConverter.instance);

        objectMapper.registerModule(simpleModule);
        // 设置为空的字段不返回
        objectMapper.setSerializationInclusion(NON_NULL);
        objectMapper.setSerializationInclusion(NON_EMPTY);
        // 指定json转换时间类型的时区
        objectMapper.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        // 指定返回的时间格式
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
        mappingJackson2HttpMessageConverter.setObjectMapper(objectMapper);

        return mappingJackson2HttpMessageConverter;
    }

    /**
     * 入参日期全局转换器，可应对@RequestBody场景
     */
    private static class RequestBodyDateConverter extends JsonDeserializer<Date> {
        public static final RequestBodyDateConverter instance = new RequestBodyDateConverter();
        private static final String[] FORMATS = {
                "yyyy-MM-dd",
                "yyyy-MM-dd HH:mm:ss",
                "yyyy-MM-dd HH:mm",
                "yyyy-MM",
                "yyyy/MM/dd"
        };

        @Override
        public Date deserialize(JsonParser p, DeserializationContext ctx) throws IOException {

            String text = p.getText();
            if (StringUtils.isBlank(text)) {
                return null;
            }
            Date date;
            try {
                date = DateUtils.parseDate(text, FORMATS);
            } catch (Exception e) {
                throw new IllegalArgumentException("日期格式不正确 '" + text + "'", e);
            }
            return date;
        }

        @Override
        public Class<?> handledType() {
            return Date.class;
        }
    }
}
