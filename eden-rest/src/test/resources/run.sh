#/bin/bash

pid=`ps aux |grep -v grep |grep eden-rest |awk '{print $2}'`
if [ -n "$pid" ];then
	echo $pid
	kill -15 $pid
fi
cd /opt/phome
nohup java -Xmx512m -Xms512m -jar eden-rest-0.0.1-SNAPSHOT.jar --spring.profiles.active=test &
