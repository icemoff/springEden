package org.bjf.test;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.bjf.utils.BgExcelReader;
import org.bjf.utils.BgExcelWriter;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author bjf on 2021/3/12.
 * @version 1.022222
 */
public class TestExcel {

    @Test
    public void test00() {
        Map<String, String> map = new HashMap<>();
        map.put("aaa", "aaa");
        map.forEach((k, v) -> {

            v = "bbbb";
        });
        for (String value : map.values()) {
            System.out.println("value = " + value);
        }
    }

    @Test
    public void test01() {

        //空白工作薄
        BgExcelWriter excelWriter = BgExcelWriter.getInstance().newBlankBook();

        // 创建多个sheet
        for (int j = 0; j < 2; j++) {
            // 创建sheet
            excelWriter.createSheet((j + 1) + "班成绩");
            //创建行
            excelWriter.createRow("姓名", "身份证号", "年龄");
            for (int i = 0; i < 10; i++) {
                //创建行
                excelWriter.createRow();
                //创建列
                excelWriter.createCell("abc");
                excelWriter.createCell("480112");
                excelWriter.createCell(10 + i);
            }
        }
        excelWriter.writeToFile("F:\\test\\aaa.xlsx");

    }

    @Test
    public void test02() {
        //从excel模板创建
        BgExcelWriter et = BgExcelWriter.getInstance().readTemplateByClasspath("/excel/user-template.xlsx");
        // 数据会从模板里标识的datas开始写
        for (int i = 0; i < 10; i++) {
            // 创建行
            et.createRow("张三", "aaa", "1833424");
        }
        // 模板变量替换
        HashMap<String, String> dataMap = new HashMap<>();
        dataMap.put("dept", "预算管理");
        dataMap.put("date", "2021-04-30");
        et.replaceFinalData(dataMap);

        et.writeToFile("F:\\test\\bbb.xlsx");
    }

    @Test
    public void test021() {
        //从excel模板创建
        BgExcelWriter et = BgExcelWriter.getInstance().readTemplateByClasspath("/excel/user-template.xlsx");

        // 根据模板创建多个sheet
        for (int j = 0; j < 3; j++) {
            et.cloneSheetByTemplate((j + 1) + "班成绩");
            for (int i = 0; i < 10; i++) {
                // 创建行
                et.createRow("张三", "aaa", "1833424");
            }
        }
        // 模板变量替换
        HashMap<String, String> dataMap = new HashMap<>();
        dataMap.put("dept", "预算管理");
        dataMap.put("date", "2021-04-30");
        et.replaceFinalData(dataMap);

        // 删除模板Sheet
        et.deleteTemplateSheet();

        et.writeToFile("F:\\test\\fff.xlsx");
    }

    @Test
    public void test03() {
        BgExcelWriter et = BgExcelWriter.getInstance().newBlankBook();

        et.createSheet("test");
        et.createRow("姓名", "身份证号", "年龄");
        CellStyle style = et.createCellStyle();

        for (int i = 0; i < 10; i++) {
            et.createRow();
            Cell c1 = et.createCell("张三");
            //单独设置单元格样式
            c1.setCellStyle(style);
            et.createCell("450888");
            et.createCell(18);
        }

        //======设置区域单元格样式(黎子扬要的)
        CellStyle cellStyle = et.createCellStyle();
        cellStyle.setBorderTop(BorderStyle.THIN);
        cellStyle.setFillBackgroundColor((short) 1);

        et.setRegionStyle(new Point(8, 3), new Point(10, 10), cellStyle);

        et.writeToFile("F:\\test\\ccc.xlsx");

    }


    @Test
    public void test04() {
        try {
            BgExcelReader eu = BgExcelReader.getInstance().readByInputstream(new FileInputStream(new File("F:\\test\\fff.xlsx")));
            List<List<String>> datas = eu.read(2, 0, 2);
            for (List<String> row : datas) {
                System.out.println(row + "----:" + row.size());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
