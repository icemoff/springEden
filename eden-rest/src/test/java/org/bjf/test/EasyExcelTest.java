package org.bjf.test;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.junit.jupiter.api.Test;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest
public class EasyExcelTest {

    @Test
    public void testString() throws Exception {

        List<List<String>> data = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            List<String> item = new ArrayList<>();
            item.add("item0" + i);
            item.add("item1" + i);
            item.add("item2" + i);
            data.add(item);
        }
        List<List<String>> head = new ArrayList<>();
        List<String> headCoulumn1 = new ArrayList<>();
        List<String> headCoulumn2 = new ArrayList<>();
        List<String> headCoulumn3 = new ArrayList<>();
        headCoulumn1.add("第一列");
        headCoulumn2.add("第二列");
        headCoulumn3.add("第三列");
        head.add(headCoulumn1);
        head.add(headCoulumn2);
        head.add(headCoulumn3);


        OutputStream out = new FileOutputStream("c:/bjf/test/ali.xlsx");
        EasyExcel.write(out)
                .sheet("班级成绩")
                .head(head)
                .doWrite(data);
    }

    @Test
    public void testBean() throws Exception {

        List<ExcelXX> data = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            ExcelXX item = new ExcelXX();
            item.name = "name" + i;
            item.age = "age" + i;
            item.email = "email" + i;
            item.address = "address" + i;
            item.sax = "sax" + i;
            item.heigh = "heigh" + i;
            item.last = "last" + i;
            data.add(item);
        }
        OutputStream out = new FileOutputStream("c:/bjf/test/aliBean.xlsx");
        EasyExcel.write(out)
                .sheet("班级成绩")
                .doWrite(data);
    }

    @Data
    @Accessors(chain = true)
    public static class ExcelXX {

        @ExcelProperty(value = "姓名", index = 0)
        private String name;

        @ExcelProperty(value = "年龄", index = 1)
        private String age;

        @ExcelProperty(value = "邮箱", index = 2)
        private String email;

        @ExcelProperty(value = "地址", index = 3)
        private String address;

        @ExcelProperty(value = "性别", index = 4)
        private String sax;

        @ExcelProperty(value = "高度", index = 5)
        private String heigh;

        @ExcelProperty(value = "备注", index = 6)
        private String last;
    }
}
