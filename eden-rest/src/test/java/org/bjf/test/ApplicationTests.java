package org.bjf.test;

import org.bjf.Application;
import org.bjf.modules.sys.service.SysUserService;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest(classes = Application.class)
public class ApplicationTests {
    @MockBean
    private SysUserService userService;

}