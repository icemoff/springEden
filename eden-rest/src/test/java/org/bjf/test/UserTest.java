package org.bjf.test;

import org.bjf.modules.core.web.core.PageVO;
import org.bjf.modules.user.bean.User;
import org.bjf.modules.user.query.UserQuery;
import org.bjf.modules.user.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class UserTest {

    @Autowired
    private UserService userService;

    @Test
    public void add() {
        User user = new User();
        user.setUsername("zs");
        userService.add(user);

        System.out.println(userService.count(new UserQuery()));

    }

    @Test
    public void addBatch() {
        User user = new User();
        user.setUsername("ls");

        User user2 = new User();
        user2.setUsername("ww");

        userService.addBatch(Arrays.asList(user, user2));

        System.out.println(userService.count(new UserQuery()));

    }

    @Test
    public void listAll() {
        List<User> users = userService.listAll(new UserQuery());

        users.forEach(System.out::println);

    }

    @Test
    public void list() {
        List<User> users = userService.list(new UserQuery());

        users.forEach(System.out::println);

    }

    @Test
    public void listPage() {
        PageVO<User> page = userService.listPage(new UserQuery());

        page.getRecords().forEach(System.out::println);

    }


}