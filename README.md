# 简介
  - 按优雅极简原则，轻量集成，适度封装，快速开发
  
## 技术栈
  1. spring-boot 2.x
  2. mybatis-plus文档：https://mybatis.plus/
  3. jdk8
  4. lombok

## 功能示例列表：
  1. 使用mybatis-plus封装BaseService，省去大量增删改查工作，具体看UserService示例
  2. 多数据源配置使用包名区分
  3. elastic-job示例
  4. sharding-jdbc读写分离示例
  5. spring-cache框架多缓存策略示例，支持3种（根据CacheNameConfig.java路由）
      - caffeine本地缓存，支持redis发布订阅删除
      - redis缓存，支持redis服务异常继续走业务逻辑
      - caffeine + redis 二级缓存，支持redis发布订阅删除一级缓存，redis服务异常继续走业务逻辑
  6. 异常体系封装示例
  7. redisKey封装使用示例
  8. JWT token认证机制示例
  9. ACL 通用权限管理
  10. 基于flowable的通用审批流程（类钉钉审批）
  11. 基于redis lua脚本的分布式锁
  12. 基于redis的全局布隆过滤
  13. retryUtil
  14. 全局traceId
  15. redis排行榜封装
  16. 基于JWT的前后分离图片验证码
  17. 奖券兑换码(14位，后2位校验位)

## 部署说明：

- maven package 打包工程
- 运行 java -jar eden-rest-0.0.1-SNAPSHOT.jar

## 命名规范（参考阿里巴巴Java开发手册）
- 获取单个对象的方法用 get 做前缀
- 获取多个对象的方法用 list 做前缀
- 获取统计值的方法用 count 做前缀
- 插入的方法用 save(推荐) 或 insert 做前缀
- 删除的方法用 remove(推荐) 或 delete 做前缀
- 修改的方法用 update 做前缀

## 应用分层（参考阿里巴巴Java开发手册）
![image](https://images.gitee.com/uploads/images/2020/0104/001953_a2252976_562480.png)

