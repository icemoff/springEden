# OOM问题排查一般步骤

1. top
   - 找到消耗cpu的进程
2. top -p pid -H
   - 列出进程中线程列表，定位资源占用情况
   - 示例： top -p 8888 -H
3. printf  "%x\n" tid
   - 把第二步查出来的线程id转换成16进制
   - 示例：printf "%x\n" 9999
4. jstack pid | grep tid
   - 查看线程堆栈信息
   - 示例：jstack 8888 | grep 3w51
   - 某些情况下无法成功打印，可以使用kill -3 pid结束进程，会输出线程信息到console。此步骤没有经过测试
   - 如:信息中waiting for monitor entry 出现连续多次在同一个或者多个线程上，说明锁竞争激烈
5. jstat -gcutil pid milliscond
   - 查看进程gc情况
   - 示例：top gcutil 8888 1000,每1000毫秒刷新一次gc情况
6. jmap -dump:format=b,file=path pid
   - 导出heapdump文件
   - 示例：jmap -dump:format=b,file=/tmp/dump.bin 8888
   - 查看分析dump文件工具：jprofiler(推荐),jhat,mat,ibm analyze

### 堆信息查看
1. jmap -heap 8888 查看整个jvm内存状态
   - 有可能会导致java进程挂起
2. jmap -histo 8888 查看对象详细占用情况
   - jmap -histo:live 8888 只统计存活对象
3. jhat -J-Xmx1024M /my.dump, 启动web服务查看jmap导出的java程序的jvm信息。
4. 使用jvisualvm ，点击文件->装入，在文件类型那一栏选择dump类型