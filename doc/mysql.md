## mysql问题定位
### 查询当前线程状态
- show status like  'Threads%';
### 查看当前正在执行中的sql
- select * from information_schema.processlist where command <> 'Sleep' order by time desc;
### 查看mysql查询配置
- show global variables like '%query%';
