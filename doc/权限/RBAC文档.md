# 系统设计

![RBAC权限ER图](./RBAC权限ER图.png)

# 访问接口

## 系统应用管理

**新增：**

- ` http://xx.com/admin/sysapp/add `

**更新：**

- ` http://xx.com/admin/sysapp/update `

**删除：**

- ` http://xx.com/admin/sysapp/delete?id=22 `

**请求方式：**

- POST

**参数：**

| 参数名      |必选|类型| 说明             |
| ---- | ---- | ---- | ---- |
| id       |否 |long | ID，删除更新的时候需要 |
| name |是 |string | 菜单名称           |
| orderNum |是 |int | 排序,越小越前         |

**返回示例**

``` 
{
    "code":200,
    "data":"",
    "message":"",
    "timestamp":1521363590660
}
```

**分页列表：**

- ` http://xx.com/admin/sysapp/listPage `

**查询全部列表：**

- ` http://xx.com/admin/sysapp/listAll `

**请求方式：**

- POST

**参数：**

| 参数名      |必选|类型| 说明  |
|:---------|:---|:----- |-----|
| page     |否  |int | 第几页 |
| pageSize |否  |int | 分页大小 |

**返回示例**

``` 
{
	"code":200,
	"data":{
		"page":1,
		"pageSize":20,
		"records":[
			{
				"ctime":"2018-10-22 22:59:46",
				"id":"1",
				"name":"开发中心",
				"orderNum":99,
				"utime":"2018-10-22 22:59:46"
			}
		],
		"total":"1",
		"totalPage":"1"
	},
	"msg":"",
	"timestamp":"1540220594727"
}
```

## 菜单管理

**新增：**

- ` http://xx.com/admin/menu/add `

**更新：**

- ` http://xx.com/admin/menu/update `

**删除：**

- ` http://xx.com/admin/menu/delete?id=22 `

**请求方式：**

- POST

**参数：**

| 参数名      |必选|类型| 说明             |
|:---------|:---|:----- |----------------|
| id       |是  |long | 菜单ID，删除更新的时候需要 |
| pid      |是  |long | 父菜单ID          |
| sysAppId      |是  |long | 所属应用id         |
| menuName |是  |string | 菜单名称           |
| menuUrl  |是  |string | 菜单URL          |
| orderNum |是  |int | 排序,越小越前 |

**返回示例**

``` 
{
    "code":200,
    "data":"",
    "message":"",
    "timestamp":1521363590660
}
```

**分页列表：**

- ` http://xx.com/admin/memu/listPage `

**查询全部列表：**

- ` http://xx.com/admin/memu/listAll `

**请求方式：**

- POST

**参数：**

| 参数名      |必选|类型| 说明     |
|:---------|:---|:----- |--------|
| pid      |否 |int | 父菜单ID  |
| sysAppId |否 |int | 系统应用id |
| menuName |否 |string | 名称     |
| page     |否  |int | 第几页    |
| pageSize |否  |int | 分页大小   |

**返回示例**

``` 
{
	"code":200,
	"data":{
		"page":1,
		"pageSize":20,
		"records":[
			{
				"ctime":"2018-10-22 22:59:46",
				"id":"1",
				"pid":"0",
				"menuName":"开发中心",
				"orderNum":99,
				"utime":"2018-10-22 22:59:46"
			}
		],
		"total":"1",
		"totalPage":"1"
	},
	"msg":"",
	"timestamp":"1540220594727"
}
```

## 角色管理

**新增：**

- ` http://xx.com/admin/role/add `

**更新：**

- ` http://xx.com/admin/role/update `

**删除：**

- ` http://xx.com/admin/role/delete?id=22 `

**请求方式：**

- POST

**参数：**

| 参数名      |必选|类型| 说明           |
|:---------|:---|:----- |--------------|
| id       |是  |long | ID，删除更新的时候需要 |
| code     |是  |long | 角色编码，不能重复    |
| roleName |是  |long | 角色名称         |

**返回示例**

``` 
{
    "code":200,
    "data":"",
    "message":"",
    "timestamp":1521363590660
}
```

**分页列表：**

- ` http://xx.com/admin/role/listPage `

**查询全部列表：**

- ` http://xx.com/admin/role/listAll `

**请求方式：**

- POST

**参数：**

| 参数名      |必选|类型| 说明   |
|:---------|:---|:----- |------|
| code |否 |string | 角色编码 |
| roleName |否 |string | 角色名称 |
| page     |否  |int | 第几页  |
| pageSize |否  |int | 分页大小 |

**返回示例**

``` 
{
	"code":200,
	"data":{
		"page":1,
		"pageSize":20,
		"records":[
			{
				"ctime":"2018-10-22 22:59:46",
				"id":"1",
				"code":"super-admin",
				"roleName":"超级管理员",
				"utime":"2018-10-22 22:59:46"
			}
		],
		"total":"1",
		"totalPage":"1"
	},
	"msg":"",
	"timestamp":"1540220594727"
}
```

## 权限管理

### 用户分配角色（覆盖式分配）

- ` http://xx.com/admin/sys-user/update-role `

**请求方式：**

- POST

**参数：**

| 参数名     |必选|类型|说明|
|:--------|:---|:----- |-----   |
| userId  |是  |long |用户ID   |
| roleIds |是  |String |角色ID，多个逗号分割   |

**返回示例**

``` 
{
    "code":200,
    "data":"",
    "message":"",
    "timestamp":1521363590660
}
```

### 查询用户的所有角色

- ` http://xx.com/admin/role/listByUser `

**请求方式：**

- POST

**参数：**

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
| userId  |是  |long |用户ID   |

**返回示例**

``` 
{
	"code":200,
	"data":[
		{
			"ctime":"2018-11-05 22:06:59",
			"roleId":"1",
			"roleName":"超级管理员",
			"status":1,
			"utime":"2018-11-05 22:06:59"
		}
	],
	"msg":"",
	"timestamp":"1541520183714"
}
```

### 角色分配菜单

- ` http://xx.com/admin/role/update-menu `

**请求方式：**

- POST

**参数：**

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|roleId |是  |long |角色ID   |
|menuIds |是  |string |菜单id,多个用逗号分割  |

**返回示例**

``` 
{
    "code":200,
    "data":"",
    "message":"",
    "timestamp":1521363590660
}
```

### 查询角色的所有菜单：

**请求URL：**

- ` http://xx.com/admin/menu/listByRole`

**请求方式：**

- POST

**参数：**

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|roleId |是  |long |角色ID   |

**返回示例**

``` 
{
    "code": 200,
    "data": [
        {
            "assigned": 1,	//1-已分配，表示角色拥有些菜单  0-未分配
            "ctime": "2018-07-04 12:12:18",
            "display": 1,
            "icon": "",
            "menuName": "组织机构管理",
            "menuUrl": "",
            "orderNum": "99",
            "pid": "0",		//父菜单ID
            "sysMenuId": "1",
            "utime": "2018-07-04 12:12:18"
        }
    ],
    "msg": "",
    "timestamp": "1530761301250"
}
```

### 查询登录用户的菜单：

**请求URL：**

- ` http://xx.com/admin/menu/listByLoginUser `

**请求方式：**

- POST

**参数：**

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |

**返回示例**

``` 
{
    "code": 200,
    "data": [
        {
            "ctime": "2018-07-04 12:12:18",
            "display": 1,
            "icon": "",
            "menuName": "组织机构管理",
            "menuUrl": "",
            "orderNum": "99",
            "pid": "0",		//父菜单ID
            "sysMenuId": "1",
            "utime": "2018-07-04 12:12:18"
        }
    ],
    "msg": "",
    "timestamp": "1530761301250"
}
```

### 查询角色的权限列表：

**请求URL：**

- ` http://xx.com/admin/role/listPerm `

**请求方式：**

- POST

**参数：**

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|roleId |是  |long |角色ID   |

**返回示例**

``` 
{
    "code":200,
    "data":[
        {
            "code":"menu",
            "desc":"菜单管理",
            "permMethods":[
                {
                    "methCode":"create",
                    "methodDesc":"新增",
                    "assigned":0  //1-已分配  0-未分配
                },
                {
                    "methCode":"read",
                    "methodDesc":"读取",
                    "assigned":0
                },
                {
                    "methCode":"update",
                    "methodDesc":"修改",
                    "assigned":0
                },
                {
                    "methCode":"delete",
                    "methodDesc":"删除",
                    "assigned":0
                }
            ]
        }
    ],
    "timestamp":"1673410806138"
}
```

### 全量更新角色权限

**请求URL：**

- ` http://xx.com/admin/role/updatePerms/{roleId} `  // roleId为路径变量

**请求方式：**

- application/json

**参数：**

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|roleId |是  |long |角色ID,路径变量   |

** application/json 内容 **

```
[
	{
		"authModuleCode":"menu",		//权限模块
		"permMethodCode":"add"          //权限方法
	},
	{
		"authModuleCode":"menu",		//权限模块
		"permMethodCode":"delete"          //权限方法
	}
]
```

### 查询角色的权限列表：

**请求URL：**

- ` http://xx.com/admin/role/listPerm `

**请求方式：**

- POST

**参数：**

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|roleId |是  |long |角色ID   |

**返回示例**

``` 
{
    "code":200,
    "data":[
        {
            "code":"menu",
            "desc":"菜单管理",
            "permMethods":[
                {
                    "methCode":"create",
                    "methodDesc":"新增",
                    "assigned":0  //1-已分配  0-未分配
                },
                {
                    "methCode":"read",
                    "methodDesc":"读取",
                    "assigned":0
                },
                {
                    "methCode":"update",
                    "methodDesc":"修改",
                    "assigned":0
                },
                {
                    "methCode":"delete",
                    "methodDesc":"删除",
                    "assigned":0
                }
            ]
        }
    ],
    "timestamp":"1673410806138"
}
```

### 按钮权限（前端显示控制）

**简要描述：**

- 后台查询回所有权限
- 前端依据权限集合判断按钮是否显示。如vue：`<button v-if="arr.indexOf('user-delete') > -1">删除按钮</button>`

**请求URL：**

- ` http://xx.com/admin/sys-user/listPerm `

**请求方式：**

- POST

**返回示例**

```
{
    "code":200,
    "data":[
        "user-add",
		"user-delete"
    ],
    "timestamp":"1673410806138"
}

```