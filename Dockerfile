#FROM java:8
FROM openjdk:8-jdk-alpine
LABEL maintainer="qianshu@gmail.com"
VOLUME /tmp

ARG JAR_FILE
ADD ${JAR_FILE} app.jar

EXPOSE 9095
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar","--spring.profiles.active=prod"]